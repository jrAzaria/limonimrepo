/****************************************
* Student Name:Yonatan Azaria*
* Exercise Name:Multi-Threading*
* File description:Player - a player on the field that can do anything he's told to*
****************************************/
#include "player.h"
#include <string>

#define DASH "(dash "
#define TURN "(turn "
#define KICK "(kick "
#define MOVE "(move "
#define SAY "(say "
#define CATCH "(catch "
#define SCORE "(score)"
#define MINUS "-"
#define END_COMMAND ")\0"
#define GAP " "


/*************************************************************************
* function name:Player*
* The Input:              *
* The output:object player*
* The Function operation:consructs a player object containing his sideand a client*
*************************************************************************/
Player::Player(UDP * playerClient, char playerSide)
{
    //init side
    side = playerSide;
    //get client
    client = playerClient;
    //init kick
    kick.first = NULL;
    kick.second = NULL;
}

/*************************************************************************
* function name:Player*
* The Input:UDP client, side, location x, location y*
* The output:object player*
* The Function operation:consructs a player object containing his side, client and location*
*************************************************************************/
Player::Player(UDP * playerClient, char playerSide, const char *x, const char *y)
{
    //init side
    side = playerSide;
    //get client
    client = playerClient;
    //init kick
    kick.first = NULL;
    kick.second = NULL;
    //place player on board
    string command = MOVE;
    command += x;
    command += GAP;
    command += y;
    command += END_COMMAND;
    //send command to server
    client->deliver(command.c_str(), command.size()+1);
}
/*************************************************************************
* function name:operator+*
* The Input:cons int power to dash*
* The output:           *
* The Function operation:dashes with inputed power by sending the right message thru client*
*************************************************************************/
void Player::operator +(const char *power)
{
    string command = DASH;
    command += power;
    command += END_COMMAND;
    client->deliver(command.c_str(), command.size()+1);
}
/*************************************************************************
* function name:operator-*
* The Input:cons int power to dash*
* The output:           *
* The Function operation:dashes backwards with inputed power by sending the right message thru client*
*************************************************************************/
void Player::operator -(const char *power)
{
    string command = DASH;
    command += MINUS;
    command += power;
    command += END_COMMAND;
    client->deliver(command.c_str(), command.size()+1);
}
/*************************************************************************
* function name:operator<*
* The Input:const char angles CCW*
* The output:           *
* The Function operation:turns CCW by inputed angles*
*************************************************************************/
void Player::operator < (const char *angle)
{
    string command = TURN;
    command += MINUS;
    command += angle;
    command += END_COMMAND;
    client->deliver(command.c_str(), command.size()+1);
}
/*************************************************************************
* function name:operator>*
* The Input:const char angles CW*
* The output:           *
* The Function operation:turns CW by inputed angles*
*************************************************************************/
void Player::operator > (const char* angle)
{
    string command = TURN;
    command += angle;
    command += END_COMMAND;
    client->deliver(command.c_str(), command.size()+1);
}
/*************************************************************************
* function name:operator<=*
* The Input:const char power*
* The output:*this so it can be connected to >= (kick direction)*
* The Function operation:gets kick power*
*************************************************************************/
Player& Player::operator <= (const char *power)
{
    kick.first = power;
    return *this;
}
/*************************************************************************
* function name:operator>=*
* The Input:const char power*
* The output:       *
* The Function operation:gets kick direction*
*************************************************************************/
void Player::operator >= (const char *direction)
{
    //check that first power was set
    if (kick.first!=NULL)
    {
        //make command
        kick.second = direction;
        string command = KICK;
        command += kick.first;
        command += GAP;
        command += kick.second;
        command += END_COMMAND;
        client->deliver(command.c_str(), command.size()+1);
    }
    //init kick
    kick.first = NULL;
    kick.second = NULL;
}
/*************************************************************************
* function name:say*
* The Input:const char power*
* The output:       *
* The Function operation:says a message*
*************************************************************************/
void Player::say(const char *message)
{
    //build the command
    string command = SAY;
    command += message;
    command += END_COMMAND;

    client->deliver(command.c_str(), command.size() + 1);
}

/*************************************************************************
* function name:checkScore*
* The Input:            *
* The output:       *
* The Function operation:asks the server for the score*
*************************************************************************/
void Player::checkScore()
{
    client->deliver(SCORE, sizeof(SCORE));
}
/*************************************************************************
* function name:checkScore*
* The Input:            *
* The output:       *
* The Function operation:checks the score*
*************************************************************************/
void Player::catchBall(const char * direction)
{
    //build command
    string command = CATCH;
    command += direction;
    command += END_COMMAND;
}
