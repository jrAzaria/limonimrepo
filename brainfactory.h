

#ifndef BRAINFACTORY_H_
#define BRAINFACTORY_H_


#include "brain.h"
#include "goalkeeper.h"
#include "striker.h"
#include "defender.h"
#include "midfielder.h"

class BrainFactory{
    public:
        enum brainType {GOALKEEPER, DEFENDER, MIDFIELDER, STRIKER};
        enum brainLocation {FIRST, SECOND, THIRD, FOURTH};
        BrainFactory();
        virtual ~BrainFactory();
        Brain * createBrain(int brainType, int brainLocation);
};

#endif /* BRAINFACTORY_H_ */
