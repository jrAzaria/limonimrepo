#ifndef MIDFIELDER_H
#define MIDFIELDER_H
#include "brain.h"

class Midfielder : public Brain
{
public:
    /*************************************************************************
    * function name:constructor*
    * The Input:int int int int string string string string*
    * The output:           *
    * The Function operation:makes a brain for a midfielder and make bounds, x & y for move, sets defense and offense points*
    *************************************************************************/
    Midfielder(pair<double, double> infX, pair<double, double> supX, pair<double, double> infY, pair<double, double> supY, string moveX, string moveY, pair<string, string> defense, pair<string,string> offense);
};

#endif // MIDFIELDER_H
