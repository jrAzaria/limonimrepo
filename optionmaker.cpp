#include "optionmaker.h"
#include "stdlib.h"
#include <math.h>
enum {tmMtNrBll, rvlNearBll, seeGl, tmMtNr, bllKck, bllNr, otOfBnds};
enum {minX, maxX, minY, maxY};

#define PI 3.14159265
#define TRIANGLE 180

#define BALL "b"
#define GOAL "g"
#define FLAG "f"
#define TOP "t"
#define BOTTOM "b"
#define LINE "l"
#define TOP_LINE "l t"
#define BOTTOM_LINE "l b"
#define WIDEST "30"
#define LEFT 'l'
#define RIGHT 'r'

#define DISTANCE 0
#define DIRECTION 1
#define VIEW_ANGLE 15
#define CORNER_SIGHT 32
#define MIDDLE_ANGLE 68
#define HALF_TURN 180
#define SEARCH "45"
#define TEAM_NAME "limonim"
#define TEAM_MATE "p \"limonim\""
#define PLAYER "p"
#define FLAG_PANELTY "f p"
#define SPACE " "
#define KICK_MARGIN 0.6
#define MAX_KICK "100"
#define ZERO 0
#define ZERO_CHAR "0"
#define TOO_CLOSE 5

//flags for triangulation
#define UP_MINUS "f t l"
#define UP_ADD "f t r"
#define DOWN_MINUS "f b r"
#define DOWN_ADD "f b l"
#define LEFT_MINUS "f l b"
#define LEFT_ADD "f l t"
#define RIGHT_MINUS "f r t"
#define RIGHT_ADD "f r b"

#define WIDTH_LIMIT 40
#define LENGTH_LIMIT 57

#define UNKNOWN_BALL 50

#define HIGH_MOTIVATION 50

/*************************************************************************
* function name:constructor*
* The Input:vector<string>, char, char*
* The output:                       *
* The Function operation:constructs an optins maker base on the brain's parameters and functions*
*************************************************************************/
OptionMaker::OptionMaker(vector<string> brainParameters, map<string, vector<int> > functionParameters , char ourSide, char theirSide)
{
    parameters = brainParameters;
    functions = functionParameters;

    side = ourSide;
    otherSide = theirSide;
    //build goal
    myGoal = GOAL;
    myGoal += SPACE;
    myGoal += otherSide;

    //build flags
    rivalFlag = FLAG;
    rivalFlag += SPACE;
    rivalFlag += otherSide;
    rivalFlag += SPACE;

    ourFlag = FLAG;
    ourFlag += SPACE;
    ourFlag += side;
    ourFlag += SPACE;

    //build flags for triangulation, space to diff from corner flags
    trianFlags.push_back(UP_MINUS SPACE);
    trianFlags.push_back(DOWN_MINUS SPACE);
    trianFlags.push_back(LEFT_MINUS SPACE);
    trianFlags.push_back(RIGHT_MINUS SPACE);

    trianFlags.push_back(UP_ADD SPACE);
    trianFlags.push_back(DOWN_ADD SPACE);
    trianFlags.push_back(LEFT_ADD SPACE);
    trianFlags.push_back(RIGHT_ADD SPACE);
}

/*************************************************************************
* function name:getOptions*
* The Input:vector<vector<string>>*
* The output:std::map <void*, int>*
* The Function operation:gets all possible options, their variable and their price(time) and puts in a vector*
*************************************************************************/
string OptionMaker::getOptions(map<string, vector<string> > see, map<string, vector<string> > sense, vector<double> playerLimits, bool ballKickable)
{
    //init all boolean
    rivalNextToBall = false;
    teamMateNextToBall = false;
    ballLocation = false;
    goalLocation = false;
    towardsRival = false;
    teamMateLocation = false;
    closestTeamMateNumFound = false;
    playerCoordsFound = false;

    seeData = see;
    senseData = sense;
    bounds = playerLimits;

    //ball is kickable
    kickable = ballKickable;

    //get ball location
    parseBall();

    //get goal location
    parseGoal();

    //find player's direction
    parseDirection();

    //get players
    parsePlayers();

    //get player's coord
    parseLimits();

    map<string, vector<int> >::const_iterator iterFunctions;

    int bestPrice = 0;

    string bestFunction;

    for (iterFunctions = functions.begin(); iterFunctions!= functions.end(); ++iterFunctions)
    {
        string functionName = iterFunctions->first;
        vector<int> vars = iterFunctions->second;
        //get price
        int price = calculatePrice(vars);

        if (price >= bestPrice)
        {
            bestPrice = price;
            bestFunction = functionName;
        }

    }

    return bestFunction;
}

/*************************************************************************
* function name:getBallLocation*
* The Input:                *
* The output:pair <float, int>*
* The Function operation:returns the location of ball*
*************************************************************************/
pair<float, string> OptionMaker::getBallLocation()
{
    return (ball);
}

/*************************************************************************
* function name:getGoalLocation*
* The Input:                *
* The output:pair <float, int>*
* The Function operation:returns the location of the goal*
*************************************************************************/
pair<unsigned int, string> OptionMaker::getGoalLocation()
{
    return (goal);
}
/*************************************************************************
* function name:getTeamMateLocation*
* The Input:                *
* The output:pair <float, int>*
* The Function operation:returns the location of closest team mate*
*************************************************************************/
pair<unsigned int, string> OptionMaker::getTeamMateLocation()
{
    return (closestTeamMate);
}
/*************************************************************************
* function name:getTeamMateNumber*
* The Input:                *
* The output:string*
* The Function operation:returns the number of the closest team mate*
*************************************************************************/
string OptionMaker::getTeamMateNumber()
{
    return (closestTeamMateNum);
}

/*************************************************************************
* function name:checkGoalfound*
* The Input:                *
* The output:bool*
* The Function operation:returns if the location of the goal was found*
*************************************************************************/
bool OptionMaker::checkGoalfound()
{
    return (goalLocation);
}

/*************************************************************************
* function name:checkBallFound*
* The Input:                *
* The output:bool*
* The Function operation:returns if the location of ball was found*
*************************************************************************/
bool OptionMaker::checkBallFound()
{
    return (ballLocation);
}

/*************************************************************************
* function name:checkTeamMateFound*
* The Input:                *
* The output:bool*
* The Function operation:returns if the location of closest team mate was found*
*************************************************************************/
bool OptionMaker::checkTeamMateFound()
{
    return (teamMateLocation);
}
/*************************************************************************
* function name:checkTeamMateNumberFound*
* The Input:                *
* The output:bool*
* The Function operation:returns if the number of the closest team mate was found*
*************************************************************************/
bool OptionMaker::checkTeamMateNumberFound()
{
    return (closestTeamMateNumFound);
}

/*************************************************************************
* function name:parsePlayerNum*
* The Input: string playerName*
* The output:                       *
* The Function operation:parses the player's number*
*************************************************************************/
string OptionMaker::parsePlayerNum(string playerName)
{
    string number;
    //if we have a number
    if (playerName.size() > sizeof(TEAM_MATE))
    {
        //find the number
        unsigned int numberLoc = playerName.find_first_of(TEAM_NAME) + sizeof(TEAM_NAME) + 1;
        number = playerName.substr(numberLoc);
    }

    return number;
}

/*************************************************************************
* function name:parseplayers*
* The Input:                        *
* The output:                       *
* The Function operation:parses the players, checks teammates locations and if any player is next to the ball*
*************************************************************************/
void OptionMaker::parsePlayers()
{
    std::pair<unsigned int, string> leftTeamMate;
    bool leftTeamMateLocation = false;
    string leftTeamMateNum;

    std::pair<unsigned int, string>  rightTeamMate;
    bool rightTeamMateLocation = false;
    string rightTeamMateNum;

    vector<string> players;
    map<string, vector<string> >::const_iterator findPlayers;
    for (findPlayers = seeData.begin(); findPlayers != seeData.end(); ++findPlayers)
    {
        //if this is a player and not a flag panelty
        if (findPlayers->first.find(PLAYER) != string::npos && findPlayers->first.find(FLAG_PANELTY) == string::npos)
            players.push_back(findPlayers->first);
    }
    for (unsigned int i=0; i< players.size(); ++i)
    {
        //find distance and direction
        unsigned int distance = atoi(seeData[players[i]][DISTANCE].c_str());
        string directionStr = seeData[players[i]][DIRECTION];
        int direction = atoi(directionStr.c_str());

        //if this is a teamate
        if (players[i].find(TEAM_MATE) != string::npos)
        {
            //check if he's next to ball
            if (ballLocation && nextToBall(distance, ball.first, abs(direction - atoi(ball.second.c_str()))))
            {
                teamMateNextToBall = true;
            }

            string playerNumber = parsePlayerNum(players[i]);

            //if player is left of him
            if (direction <= ZERO)
            {
                //if one wasn't found or this one is closer -
                if (!leftTeamMateLocation || leftTeamMate.first > distance)
                {

                    leftTeamMate.first = distance;
                    leftTeamMate.second = directionStr;
                    leftTeamMateLocation = true;
                    leftTeamMateNum = playerNumber;
                }
            }
            else
            {
                //if one wasn't found or this one is closer
                if (!rightTeamMateLocation || rightTeamMate.first > distance)
                {
                    rightTeamMate.first = distance;
                    rightTeamMate.second = directionStr;
                    rightTeamMateLocation = true;
                    rightTeamMateNum = playerNumber;
                }
            }

        }
        //if this is a rival
        else
        {
            //check if he's next to the ball (if we found the ball)
            if (ballLocation && !rivalNextToBall)
            {
                if (nextToBall(distance, ball.first, abs(direction - atoi(ball.second.c_str()))))
                {
                    rivalNextToBall = true;
                }

            }

        }
    }

    //find closest player
    if (rightTeamMateLocation && leftTeamMateLocation)
    {
        //if the player is looking left - use the rightmost teamMate
        if (playerDirection <= ZERO)
        {
            teamMateLocation = true;
            closestTeamMate = rightTeamMate;
            closestTeamMateNum = rightTeamMateNum;
            closestTeamMateNumFound = true;
        }
        //player is looking right - use the leftmost team mate
        else
        {
            teamMateLocation = true;
            closestTeamMate = leftTeamMate;
            closestTeamMateNum = leftTeamMateNum;
            closestTeamMateNumFound = true;
        }
    }
    else
        //if we only have a team mate to the right he's the closest
    if (rightTeamMateLocation)
    {
        teamMateLocation = true;
        closestTeamMate = rightTeamMate;
        closestTeamMateNum = rightTeamMateNum;
        closestTeamMateNumFound = true;
    }
    else
    //if we only have a team mate to the left he's the closest
    if (leftTeamMateLocation)
    {
        teamMateLocation = true;
        closestTeamMate = leftTeamMate;
        closestTeamMateNum = leftTeamMateNum;
        closestTeamMateNumFound = true;
    }

}


/*************************************************************************
* function name:parseBall*
* The Input:                        *
* The output:                       *
* The Function operation:parses the ball from see data*
*************************************************************************/
void OptionMaker::parseBall()
{
    //if ball is kickable
    if (kickable)
    {
        //indicate we know it's location
        ballLocation = true;

        //and that it's next to us
        ball.first = ZERO;
        ball.second = ZERO_CHAR;
    }
    //if the ball is visible
    if (seeData.count(BALL) == 1)
    {
        //get it's parameters
        ball.first = atof(seeData[BALL][DISTANCE].c_str());

        ball.second = seeData[BALL][DIRECTION].c_str();

        ballLocation = true;

        //if ball is kickable - indicate it
        if (ball.first < KICK_MARGIN)
        {
            kickable = true;
        }
    }
    else
    {
        //set to unknown
        ballLocation = false;
    }
}

/*************************************************************************
* function name:parseGoal*
* The Input:                        *
* The output:                       *
* The Function operation:parses the goal from see data*
*************************************************************************/
void OptionMaker::parseGoal()
{
    //if goal is visible
    if (seeData.count(myGoal) == 1)
    {
        goalLocation = true;
        //set location
        goal.first = atoi(seeData[myGoal][DISTANCE].c_str());

        goal.second = seeData[myGoal][DIRECTION].c_str();
    }
    else
        goalLocation = false;

}
/*************************************************************************
* function name:parseDirection*
* The Input:                        *
* The output:                       *
* The Function operation:parses the direction of the player*
*************************************************************************/
void OptionMaker::parseDirection()
{
    //if goal can be seen
    if (goalLocation)
    {
        //the player's direction is negative value of it's direction towards the goal
        playerDirection = -1 * atoi(goal.second.c_str());
        towardsRival = true;
    }
    else
    {
        //build flags
        string upperRivalFlag = rivalFlag;
        upperRivalFlag += TOP;
        string lowerRivalFlag = rivalFlag;
        lowerRivalFlag += BOTTOM;


        //if upperflag can be seen - find direction
        if (seeData.count(upperRivalFlag)==1)
        {
            int flagDirection = atoi(seeData[upperRivalFlag][DIRECTION].c_str());

            //if the top line can be see
            if (seeData.count(TOP_LINE) == 1)
            {
                //if the flag isn't see from the corner of the eye
                if (abs(flagDirection)  <= CORNER_SIGHT)
                {
                    //direction of the players is the line's negative
                    playerDirection = -1 * atoi(seeData[TOP_LINE][DIRECTION].c_str());
                    towardsRival = true;
                }
            }
            else
            {
                //player is towards rival
                towardsRival = true;

                //his direction is more or less 68 or -68 (depending on his side)
                if (otherSide == RIGHT)
                    playerDirection = -MIDDLE_ANGLE;
                else
                    playerDirection = MIDDLE_ANGLE;
            }
        }
        //if lower flag can be seen - find direction
        else
            if (seeData.count(lowerRivalFlag)==1)
            {
                int flagDirection = atoi(seeData[lowerRivalFlag][DIRECTION].c_str());

                //if the bottom line can be seen
                if (seeData.count(BOTTOM_LINE) == 1)
                {
                    if (abs(flagDirection) <= CORNER_SIGHT)
                    {
                        //direction of the player is the line's negative
                        playerDirection = -1 * atoi(seeData[BOTTOM_LINE][DIRECTION].c_str());
                        towardsRival = true;
                    }
                }
                else
                {
                    //player is towards rival
                    towardsRival = true;

                    //his direction is more or less 68 or -68 (depending on his side)
                    if (otherSide == RIGHT)
                        playerDirection = MIDDLE_ANGLE;
                    else
                        playerDirection = -MIDDLE_ANGLE;
                }
            }
    }
}
/*************************************************************************
* function name:calculateLimit*
* The Input:                        *
* The output:                       *
* The Function operation:calculates the limiting factor and the results*
*************************************************************************/
int OptionMaker::calculateLimit(float value, float distance)
{
    float score = value;
    //refrain from dividing by zero
    if ((value + distance) !=ZERO)
    {
        float limit = (value-distance)/(value + distance);
        score *= limit;
    }
    return score;
}

/*************************************************************************
* function name:calculatePrice*
* The Input:                        *
* The output:                       *
* The Function operation:calculates the price for each function*
*************************************************************************/
int OptionMaker::calculatePrice(vector<int> vars)
{

    float value;
    float distance;
    //if we don't know where is the ball init all ball related parameters to 0
    if (!ballLocation)
    {
        vars[tmMtNrBll] = 0;
        vars[rvlNearBll] = 0;
        vars[bllKck] = 0;
        vars[bllNr] = 0;
    }
    else
    //if ball isn't kickable init that parameter
    if (!kickable)
    {
        vars[bllKck] = 0;

        //add to ball near acoording to how close the ball is
        distance = ball.first;
        value = vars[bllNr];

        vars[bllNr] += calculateLimit(value, distance);
    }
    else
    //if ball is kickable - it isn't nearby, init parameter
    {
        vars[bllNr] = 0;
    }

    //init player param if we didn't find one
    if (!teamMateLocation)
    {
        vars[tmMtNrBll] = 0;
        vars[tmMtNr] = 0;
    }
    else
    //if team mate is next to ball - init parameters that relate to ball use
    if (teamMateNextToBall)
    {
        vars[bllKck] = 0;
        vars[tmMtNr] = 0;
        vars[bllNr] = 0;
    }

    //if rival isn't next to ball - init parameter
    if (!rivalNextToBall)
        vars[rvlNearBll] = 0;

    //if we aren't facing the rival - init parameters about passing ball
    if(!towardsRival)
    {
        vars[tmMtNr] = 0;
    }
    //if we are facing the rival and we found a team mate
    else if (towardsRival && teamMateLocation)
    {
        //raise it's parameters according to how close he is
        distance = closestTeamMate.first;
        value = vars[tmMtNr];

        vars[tmMtNr] += calculateLimit(value, distance);
    }

    //if we can see the goal
    if (goalLocation)
    {
        //raise the parameter according to how close we are
        distance = goal.first;
        value = vars[seeGl];

        vars[seeGl] += calculateLimit(value, distance);
    }
    //if we can't see the goal - init that parameter
    else
        vars[seeGl] = 0;

    //if we know where the player is
    if (playerCoordsFound)
    {
        //and he isn't next to the ball
        if (!kickable)
        {
            float xDiff = 0;
            float yDiff = 0;

            //find his distance from the bounds
            if(playerCoords.first < bounds[minX] || playerCoords.first > bounds[maxX])
                xDiff = min(abs(playerCoords.first - bounds[minX]), abs(playerCoords.first - bounds[maxX]));

            if (playerCoords.second < bounds[minY] || playerCoords.second > bounds[maxY])
                yDiff = min(abs(playerCoords.second - bounds[minY]), abs(playerCoords.second - bounds[maxY]));

            //find the distance
            distance = sqrt(pow(xDiff,2) + pow(yDiff,2));

            //if player is out of bounds
            if (distance > ZERO)
            {
                value = vars[otOfBnds];

                //find how much to add
                float limit = 0;
                limit = (abs(value) * distance)/(HIGH_MOTIVATION * (sqrt(pow(WIDTH_LIMIT,2) + pow(LENGTH_LIMIT,2))/2));

                vars[otOfBnds] += value * limit;
            }
            else
                //init parameter
                vars[otOfBnds] = 0;
        }

    }

    int score = 0;
    for (unsigned int i=0; i< vars.size(); ++i)
    {
        score += vars[i];
    }

    return score;
}
/*************************************************************************
* function name:parseLimit*
* The Input:            *
* The output:           *
* The Function operation:gets the player's location and limits*
*************************************************************************/
void OptionMaker::parseLimits()
{
    //iterate through map and find 2 flags that are the same
    map<string, vector<string> >::const_iterator findFlags;

    findFlags = seeData.begin();

    //if see isn't empty
    if (findFlags!= seeData.end())
    {
        string thisFlag = findFlags->first;
        ++findFlags;

        string nextFlag;

        unsigned int matchNum = 0;

        //bool to see if we found 2 good flags
        bool found = false;

        for (; findFlags != seeData.end(); ++findFlags)
        {

            //if this is a flag and it's the same as the prev one
            for (unsigned int i=0; i < trianFlags.size(); ++i)
            {
                if (findFlags->first.find(trianFlags[i]) != string::npos)
                {
                    if(thisFlag.find(trianFlags[i]) != string::npos)
                    {
                        //keep it and exit
                        nextFlag = findFlags->first;
                        found = true;
                        //find the flag that matched
                        matchNum = i;
                        break;
                    }
                    else
                    {
                        //we found this flag's type so keep it and compare to next flag
                        thisFlag = findFlags->first;
                        //leave comparsion loop
                        break;
                    }
                }
            }
            //if we found a good match - leave loop;
            if (found)
                break;
        }

        //if we found a good match
        if (found)
        {
            //get flags numerical location
            int firstFlag = atoi(thisFlag.substr(sizeof(UP_ADD)).c_str());
            int secondFlag = atoi(nextFlag.substr(sizeof(UP_ADD)).c_str());

            //get flag's distance and angle
            float distA = atof(seeData[thisFlag][DISTANCE].c_str());
            float distB = atof(seeData[nextFlag][DISTANCE].c_str());
            int angle = abs(atoi(seeData[thisFlag][DIRECTION].c_str()) - atoi(seeData[nextFlag][DIRECTION].c_str()));

            //get coordinates, check which flags to use where

            //if we are relating to the flag further away from 0
            if (matchNum < trianFlags.size()/2)
            {
                //get the bigger one to be first
                if (firstFlag < secondFlag)
                {
                    //switch distance
                    float tempDist = distB;
                    distB = distA;
                    distA = tempDist;

                    //switch flags
                    string tempFlag = thisFlag;
                    thisFlag = nextFlag;
                    nextFlag = tempFlag;
                }

            }
            //if we are relating to the flag closer to 0
            else
            {
                //get the smaller one to be first
                if (firstFlag > secondFlag)
                {
                    //switch between the distance
                    float tempDist = distB;
                    distB = distA;
                    distA = tempDist;


                    //switch flags
                    string tempFlag = thisFlag;
                    thisFlag = nextFlag;
                    nextFlag = tempFlag;
                }
            }

            playerCoords = triangulate(distA, distB, angle, thisFlag, nextFlag);
            playerCoordsFound = true;
        }

    }
}

/*************************************************************************
* function name:triangulate*
* The Input:            *
* The output:           *
* The Function operation:gets the player's location*
*************************************************************************/
pair<double, double> OptionMaker::triangulate(double distA, double distB, double angleC, string flagA, string flagB)
{
    //convert numeric to radian
    double radianC = angleC * PI / TRIANGLE;

    //find unknown distance
    double distC = sqrt(pow(distA,2) + pow(distB,2) - 2 * distA * distB * cos(radianC));

    //find radians A and B
    double radianA = acos((pow(distB,2) + pow(distC,2) - pow(distA,2))/(2*distC*distB));

    double radianB = acos((pow(distA,2) + pow(distC,2) - pow(distB,2))/(2*distC*distA));

    //find distance of player from line
    double distanceFrom = distC * ((sin(radianA) * sin(radianB))/(sin(radianA + radianB)));

    //find distance of player on line from A
    double distanceOnA = sqrt(pow(distA, 2) - pow(distanceFrom, 2));

    //find distance of player on line from B
    double distanceOnB = sqrt(pow(distB, 2) - pow(distanceFrom, 2));

    //get the distance of flag A from 0
    double lengthA = atoi(flagA.substr(sizeof(UP_ADD)).c_str());

    //get the shortened string with only the flag inside
    string onlyFlag = flagA.substr(0, sizeof(UP_ADD) - 1);

    //check location of Perpendicular
    if(distanceOnB > distanceOnA && distanceOnB > distC)
        //if out of bounds - turn it around
        distanceOnA *= -1;

    //coords to return
    pair<double, double> coords;


    //find the right flag and use it's location to find x and y
    if (onlyFlag == UP_ADD)
    {
        coords.first = lengthA + distanceOnA;
        coords.second = WIDTH_LIMIT - distanceFrom;
    }
    else if (onlyFlag == UP_MINUS)
    {
        coords.first = distanceOnA - lengthA;
        coords.second = WIDTH_LIMIT - distanceFrom;
    }
    else if (onlyFlag == DOWN_ADD)
    {
        coords.first = -(lengthA + distanceOnA);
        coords.second = - (WIDTH_LIMIT - distanceFrom);
    }
    else if (onlyFlag == DOWN_MINUS)
    {
        coords.first = lengthA - distanceOnA;
        coords.second = - (WIDTH_LIMIT - distanceFrom);
    }
    else if (onlyFlag == LEFT_ADD)
    {
        coords.first = - (LENGTH_LIMIT - distanceFrom);
        coords.second = lengthA + distanceOnA;
    }
    else if (onlyFlag == LEFT_MINUS)
    {
        coords.first = - (LENGTH_LIMIT - distanceFrom);
        coords.second = distanceOnA - lengthA;
    }
    else if (onlyFlag == RIGHT_ADD)
    {
        coords.first = LENGTH_LIMIT - distanceFrom;
        coords.second = - (distanceOnA + lengthA);
    }
    else if (onlyFlag == RIGHT_MINUS)
    {
        coords.first = LENGTH_LIMIT - distanceFrom;
        coords.second = lengthA - distanceOnA;
    }

    //return x and y coords
    return coords;
}
/*************************************************************************
* function name:nextToBall*
* The Input:            *
* The output:           *
* The Function operation:checks if a player is close to a ball*
*************************************************************************/
bool OptionMaker::nextToBall(double distBall, double distPlayer, double angleBetween)
{
    //convert numeric to radian
    double radianBetween = angleBetween * PI / TRIANGLE;

    //find unknown distance between them
    double distBetween = sqrt(pow(distBall,2) + pow(distPlayer,2) - 2 * distBall * distPlayer * cos(radianBetween));

    return (distBetween <= TOO_CLOSE);
}
