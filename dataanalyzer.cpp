#include "dataanalyzer.h"
#include "striker.h"
#include "tcp.h"
#include "brain.h"

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <utility>
#include <pthread.h>

#define START "init"
#define SENSE "sense_body"
#define SEE "see"
#define HEAR "hear"
#define SCORE "score"
#define TYPE "player_type"
#define BALL "(b"
#define GOAL "(g"
#define SEARCH "45"
#define SEARCH_NUM 45
#define CIRCLE "360"
#define FULL_SWEEP 360
#define SOFT "33"
#define MILD "66"
#define STRONG "100"
#define MAX_RUN 1.00
#define MAX_KICK 30
#define MIN_KICK_DISTZER O.6
#define LEFT 'l'
#define RIGHT 'r'
#define VIEW_ANGLE 15

#define INIT_COMMAND "(init limonim (version 15))"
#define INIT_COMMAND_GOALIE "(init limonim (version 15)(goalie))"
#define SPACE ' '
#define PORT 6000
#define NUM_OF_THREADS 3
#define AFTER_FIRST_PARENTHESIS 1
#define HALF_TIME 3000
#define GAME_OVER 6000
#define BEFORE_KICK_OFFZERO
#define SECONDS_AFTER_GOAL 4
#define SECONDS_AFTER_LOSE 2


#define UNWANTED_HEAR_MSG 2
#define NULL_STR "null"
#define N_CHAR 'n'
#define FALSE false
#define TRUE true
#define ZERO 0
#define DOUBLE_START_PARENTHESIS "(("
#define SPACE_STR " "
#define END_PARENTHESIS ")"
#define START_PARENTHESIS "("
#define DOUBLE_END_PARENTHESIS "))"
#define MERCHAOT "\""

using namespace std;

/*************************************************************************
* function name:DataAnalyzer*
* The Input:              *
* The output:       *
* The Function operation:constructor*
*************************************************************************/
DataAnalyzer::DataAnalyzer(Brain * newBrain)
{
    this->brain = newBrain;
	client = new UDP(PORT);
    this->strategy = FALSE;
    this->hearData = NULL_STR;
    this->hearDataInUse = FALSE;
    this->myScore =ZERO;
    this->onField = TRUE;
    this->otherSide = N_CHAR;
    this->seeData = NULL_STR;
    this->seeDataInUse = FALSE;
    this->senseData = NULL_STR;
    this->senseDataInUse = FALSE;
    this->side = N_CHAR;
    this->theirScore =ZERO;
    this->time =ZERO;
}
/*************************************************************************
* function name:destructor*
* The Input:            *
* The output:           *
* The Function operation:destructs the object*
*************************************************************************/
DataAnalyzer::~DataAnalyzer()
{
    //if we have a client
    if (client != NULL)
    {
        //close communcations
        client->finish();
        //erse it
        delete client;
    }

    //if we have a brain - delete it
    if (brain != NULL)
        delete brain;

}
/*************************************************************************
* function name:play*
* The Input:                    *
* The output:               *
* The Function operation:opens 2 threads and plays the game*
*************************************************************************/
void DataAnalyzer::play(bool goalie)
{
    //connect to server
    if (goalie)
    {
       client->deliver(INIT_COMMAND_GOALIE, sizeof(INIT_COMMAND_GOALIE));
    }
    else
        client->deliver(INIT_COMMAND, sizeof(INIT_COMMAND));

    void * returnStatus;
    pthread_t newThreads[NUM_OF_THREADS];

    //get side
    string openingMessage;

    //find the opening message
    do
    {
        openingMessage = client->mimicTcpReceive();
    } while (parseTitle(openingMessage)!= START);

    vector<string> empty;



    //find side
    findSide(openingMessage);

    //find other side
    if (side == LEFT)
        otherSide =RIGHT;
    else
        otherSide = LEFT;


    //get player's number
    string playerNum = parsePlayerNum(openingMessage);


    this->brain->setPlayer(empty, client, side, otherSide, playerNum);

    //get first msg to player
    hearData = parseOpeningReferee(openingMessage);

    //indicate we have hear data
    hearDataInUse = true;

    //create first thread with receiving
    int threadCheck = pthread_create(&newThreads[0], NULL, &DataAnalyzer::launchReceive, this);

    //check that thread was created
    if (threadCheck)
    {
        //cout << "thread number " << 1 << "failed with " << threadCheck << endl;
        exit (-1);
    }

    //create second thread
    threadCheck = pthread_create(&newThreads[1], NULL, &DataAnalyzer::launchDeliver, this);

    //check that thread was created
    if (threadCheck)
    {
        //cout << "thread number " << 2 << "failed with " << threadCheck << endl;
        exit (-1);
    }


    //create third thread
    threadCheck = pthread_create(&newThreads[1], NULL, &DataAnalyzer::launchHear, this);

    //check that thread was created
    if (threadCheck)
    {
        //cout << "thread number " << 3 << "failed with " << threadCheck << endl;
        exit (-1);
    }

    while (onField)
        this->brain->decideAction();


    //wait for threads to finish
    for (int i=0; i< NUM_OF_THREADS; ++i)
    {
        threadCheck = pthread_join(newThreads[i], &returnStatus);
        if (threadCheck)
        {
            //cout << "thread number " << i << "failed with " << threadCheck << endl;
            exit (-1);
        }
    }

    pthread_exit(NULL);
}
/*************************************************************************
* function name:get*
* The Input:              *
* The output:void * *
* The Function operation:gets the output from server, passes data to class vars, exits when game over*
*************************************************************************/
void*  DataAnalyzer::get()
{
    string text, title;
    do{
        //get data
        text = client->mimicTcpReceive();
        title = this->parseTitle(text);

        //get time
        time = parseTime(text);


             //data is now being used, lock
             seeDataInUse = TRUE;

        if ((title == SEE) && !seeDataInUse) {
            //copy data so it can be analyzed
            seeData = text.substr();

             //data is now being used, lock
             seeDataInUse = TRUE;

        } else if ((title == SENSE) && !seeDataInUse) {
            //copy data so it can be analyzed
            senseData = text.substr();

            //data is now being used, lock
            senseDataInUse = true;

        } else if ((title == HEAR) && !hearDataInUse) {
            //copy data so it can be analyzed
            hearData = text.substr();

            //data is now being used, lock
            hearDataInUse = true;
        }

    } while(time!=GAME_OVER && onField);
    onField = FALSE;

    pthread_exit(NULL);

}

/*************************************************************************
* function name:send*
* The Input:              *
* The output:void * *
* The Function operation:sends the parsed data*
*************************************************************************/
void * DataAnalyzer::send()
///////fix implementation
{
    //while on field
    while (this->onField)
    {
        if (seeDataInUse)
        {
            brain->getSee(parseSee(seeData));
            seeDataInUse = FALSE;
        }
        if (senseDataInUse)
        {
            brain->getSense(parseSense(senseData));
            senseDataInUse = FALSE;
        }
    }
    pthread_exit(NULL);

}

/*************************************************************************
* function name:sendHear*
* The Input:              *
* The output:void * *
* The Function operation:sends the parsed hear data*
*************************************************************************/
void* DataAnalyzer::sendHear() {
    //while on field
    while (this->onField)
    {
        //checks if we have a hear message to parse
        if(hearDataInUse) {
        //sends the parsed hear message

            vector<string> hearMsg = parseHear(hearData);
            if (hearMsg.size() != UNWANTED_HEAR_MSG)
                brain->getHear(hearMsg);
            hearDataInUse = false;
        }
    }
    pthread_exit(NULL);
}

/*************************************************************************
* function name:launchReceive*
* The Input:this*
* The output:void * *
* The Function operation:static function that helps an object launch a reaceiving thread*
*************************************************************************/
void* DataAnalyzer::launchReceive(void* thisObject)
{
	DataAnalyzer* obj = (DataAnalyzer*)(thisObject);
    return obj->get();
}

/*************************************************************************
* function name:launchDeliver*
* The Input:this*
* The output:void * *
* The Function operation:static function that helps an object launch a sending thread*
*************************************************************************/
void* DataAnalyzer::launchDeliver(void* thisObject)
{
	DataAnalyzer* obj = (DataAnalyzer*)(thisObject);
    return obj->send();
}
/*************************************************************************
* function name:launchHear
* The Input:this*
* The output:void * *
* The Function operation:static function that helps an object launch a sending hear thread*
*************************************************************************/
void* DataAnalyzer::launchHear(void* thisObject)
{
    DataAnalyzer* obj = (DataAnalyzer*)(thisObject);
    return obj->sendHear();
}

/*************************************************************************
* function name:findSide*
* The Input:string*
* The output:               *
* The Function operation:finds which side are you on*
*************************************************************************/
void DataAnalyzer::findSide(const string text)
{
    //after receving text with init command the second char will be the side
    side = text[text.find_first_of(SPACE) + 1];
}

/*************************************************************************
* function name:parseTitle*
* The Input:string*
* The output:string*
* The Function operation:returns the title of the message from server*
*************************************************************************/
string DataAnalyzer::parseTitle(const string text)
{
    string returnString;

    //find the first word
    returnString = text.substr(AFTER_FIRST_PARENTHESIS, text.find_first_of(SPACE) - 1);
    return returnString;
}

/*************************************************************************
* function name:parseTime*
* The Input:string*
* The output:unsigned int*
* The Function operation:finds the time the message was sent*
*************************************************************************/
unsigned int DataAnalyzer::parseTime(const string text)
{
    unsigned int textTime = time;

    string title = parseTitle(text);
    //if this is message has time
    if (title == SEE || title == SENSE || title == SCORE)
    {
        //find the first space
        size_t firstSpace = text.find_first_of(SPACE);
        //find the time
        string tempTime = text.substr(firstSpace + 1, text.find_first_of(SPACE,firstSpace+1) -(firstSpace+1));
        textTime = atoi(tempTime.c_str());

    }

    return textTime;
}

/*************************************************************************
* function name:parseMyScore*
* The Input:string*
* The output:unsigned int*
* The Function operation:finds my score from a score data message*
*************************************************************************/
unsigned int DataAnalyzer::parseMyScore(const string text)
{
    unsigned int score = myScore;

    //find the first space
    size_t firstSpace = text.find_first_of(SPACE);
    //find the second space
    firstSpace = text.find_first_of(SPACE, firstSpace+1);
    //find the score
    string tempScore = text.substr(firstSpace + 1, text.find_first_of(SPACE,firstSpace+1) -(firstSpace+1));
    score = atoi(tempScore.c_str());

    return score;
}

/*************************************************************************
* function name:parseTheirScore*
* The Input:string*
* The output:unsigned int*
* The Function operation:finds their score from a score data message*
*************************************************************************/
unsigned int DataAnalyzer::parseTheirScore(const string text)
{
    unsigned int score = theirScore;

    //find the first space
    size_t firstSpace = text.find_first_of(SPACE);
    //find the second space
    firstSpace = text.find_first_of(SPACE, firstSpace+1);
    //find the third space
    firstSpace = text.find_first_of(SPACE, firstSpace+1);
    //find the score
    string tempScore = text.substr(firstSpace + 1, text.find_first_of(END_PARENTHESIS) -(firstSpace+1));
    score = atoi(tempScore.c_str());

    return score;
}

/*************************************************************************
* function name:parseDistance*
* The Input:string*
* The output:string*
* The Function operation:finds the distance of player from object using the server's message*
*************************************************************************/
string DataAnalyzer::parseDistance(const string text)
{
    string returnString;

    //find the first space
    size_t firstSpace = text.find_first_of(SPACE, text.find_first_of(END_PARENTHESIS));
    //find the distance
    returnString = text.substr(firstSpace + 1, text.find_first_of(SPACE,firstSpace+1) -(firstSpace+1));
    return returnString;
}

/*************************************************************************
* function name:parseDirection*
* The Input:string*
* The output:string*
* The Function operation:finds the direction of player from object using the server's message*
*************************************************************************/
string DataAnalyzer::parseDirection(const string text)
{
    string returnString;

    //find the first space
    size_t firstSpace = text.find_first_of(SPACE, text.find_first_of(END_PARENTHESIS));
    //find the second space
    firstSpace = text.find_first_of(SPACE, firstSpace + 1);
    //find the direction
    size_t parentEnd = text.find_first_of(END_PARENTHESIS,firstSpace);
    size_t amount = text.find_first_of(SPACE,firstSpace+1);
    //if a space wasn't found use the last parenthesis
    if (amount == std::string::npos)
        amount = parentEnd;
    returnString = text.substr(firstSpace + 1, amount -(firstSpace+1));
    return returnString;
}

/**************************************************************
* function name:parseSee          *
* The Input: const string text             *
* The output: map<string, vector <string>>             *
* The Function operation: gets a see message and
* parses it into a map data-structure           *
***************************************************************/
map<string, vector<string> > DataAnalyzer::parseSee(const string text) {
	std::map<string, vector<string> > toReturn;
	std::vector<string>::iterator it;
//where we are on the string:
    unsigned int pos = ZERO;
//2 strings in which the data will be put
    string key, v;
    vector<string> value;
//will be used to say which substring to take now
    unsigned int leftLim, rightLim, len, endOfValues, nextSpace;
//runs all over the string:
    while (pos < text.length()) {
//finds the next (( and copies what's after it to the key
        leftLim = text.find(DOUBLE_START_PARENTHESIS, pos);
        if(leftLim == ZERO) {
            break;
        } else {
        leftLim += 3;
    	if(pos < leftLim) {
          rightLim = text.find(END_PARENTHESIS, leftLim);
    	  len = rightLim - leftLim;
    	  key = text.substr(leftLim, len);
          //cout<<"key: "<<key<<endl;
/**finds the next ) and copies what's before it (and after
the previous ')' ) to value*/
    	pos = rightLim + 1;
    	//leftLim = pos;
        endOfValues = text.find(END_PARENTHESIS, pos);
    	while(pos < endOfValues) {
           leftLim = text.find_first_not_of(SPACE, pos);
            nextSpace = text.find(SPACE_STR, leftLim);
    	    if (nextSpace < endOfValues) {
    	    	rightLim = nextSpace;
    	    } else {
    	    	rightLim = endOfValues;
    	    }
    	    len = rightLim - leftLim;
    	    v = text.substr(leftLim, len);
            //cout<<"v: "<<v<<endl;
    	    value.push_back(v);
    	    pos = rightLim + 1;
    	    leftLim = pos;
    	 }
    	///a check for debugging:
        //cout << "value contains:";
          for (it=value.begin(); it<value.end(); it++) {
            std::cout << ' ' << *it<<SPACE_STR;
    	  cout<<endl;
          }
 //adds the new key and value to the list
    	toReturn.insert (std::pair<string,vector<string> >(key, value));
//clears the key and the value
    	value.clear();
    	key.clear();
    	pos = rightLim + 1;
    	} else {
    		break;
    	}
    }
    }
    return toReturn;
}

/**************************************************************
* function name:parsePlayerType        *
* The Input: const string text             *
* The output: map<string, vector <string>>             *
* The Function operation: gets a playerType message and
* parses it into a map data-structure           *
***************************************************************/
map<string, vector<string> > DataAnalyzer::parsePlayerType(const string text) {
	std::map<string, vector<string> > toReturn;
	std::vector<string>::iterator it;
//where we are on the string:
    unsigned int pos = 2;
//2 strings in which the data will be put
    string key, v;
    vector<string> value;
//will be used to say which substring to take now
    unsigned int leftLim, rightLim, len;
//runs all over the string:
    while (pos < text.length()) {
//finds the next (( and copies what's after it to the key
        leftLim = text.find(START_PARENTHESIS, pos) + 1;
    	if(pos < leftLim) {
        rightLim = text.find(SPACE_STR, leftLim);
    	  len = rightLim - leftLim;
    	  key = text.substr(leftLim, len);
    ////cout for debugging
        //cout<<"key: "<<key<<endl;
/**finds the next ) and copies what's before it (and after
the previous ')' ) to value*/
    	pos = rightLim + 1;
    	//leftLim = pos;
    	leftLim = pos;
        rightLim = text.find(END_PARENTHESIS, leftLim);
    	len = rightLim - leftLim;
    	 v = text.substr(leftLim, len);
           //cout<<"v: "<<v<<endl;
    	    value.push_back(v);
    	  // pos = rightLim + 1;
    	  // leftLim = pos;
    	 //adds the new key and value to the list
    	    	toReturn.insert (std::pair<string,vector<string> >(key, value));
    	//clears the key and the value
    	    	value.clear();
    	    	key.clear();
    	    	pos = rightLim ;
    	  } else {
    		  break;
    	  }
    }
            //cout<<"parsed alright"<<endl;
    	    return toReturn;
}

/**************************************************************
* function name: parseSense        *
* The Input: const string text             *
* The output: map<string, vector <string>>             *
* The Function operation: gets a parseSense message and
* parses it into a map data-structure           *
***************************************************************/
map<string, vector<string> > DataAnalyzer::parseSense(const string text) {
	std::map<string, vector<string> > toReturn;
	std::vector<string>::iterator it;
//where we are on the string:
    unsigned int pos = 2;
//2 strings in which the data will be put
    string key, v;
    vector<string> value;
//will be used to say which substring to take now
    unsigned int leftLim, rightLim, len, endOfValues, nextOpen, nextClose, nextSpace;
//runs all over the string:
    while (pos < text.length()) {
//finds the next (( and copies what's after it to the key
        leftLim = text.find(START_PARENTHESIS, pos) + 1;
 //checks for an infinite run on the string:
    	  if(pos < leftLim) {
 //first- copies the key:
            rightLim = text.find(SPACE_STR, leftLim);
    	    len = rightLim - leftLim;
    	    key = text.substr(leftLim, len);
            //cout<<"key: "<<key<<endl;
  	    	pos = rightLim;
  	    	leftLim = pos;
/**checks for 1 of 2 conditions-
 * 1: there's an inner parenthesis:
 */
            nextOpen = text.find(START_PARENTHESIS, pos);
            nextClose = text.find(END_PARENTHESIS, pos);
    		if (nextOpen < nextClose) {
                endOfValues = text.find(DOUBLE_END_PARENTHESIS, pos);
    			while (pos < endOfValues) {
                    leftLim = text.find(START_PARENTHESIS, pos) + 1;
                    rightLim = text.find(SPACE_STR, leftLim);
    				len = rightLim - leftLim;
    				v = text.substr(leftLim, len);
    				value.push_back(v);
    				v.clear();
    				 pos = rightLim + 1;
    				 leftLim = pos;
                    rightLim = text.find(END_PARENTHESIS, leftLim);
    				len = rightLim - leftLim;
    				v = text.substr(leftLim, len);
    				value.push_back(v);
    				v.clear();
    				 pos = rightLim + 1;
    				 leftLim = pos;
    			}
		    	///a check for debugging:
                //cout << "value contains:";
                  for (it=value.begin(); it<value.end(); it++) {

                    std::cout << ' ' << *it<<SPACE_STR;
		    	  cout<<endl;
                  }
		 //adds the new key and value to the list
		    	toReturn.insert (std::pair<string,vector<string> >(key, value));
		//clears the key and the value
		    	value.clear();
		    	key.clear();
		    	pos = rightLim + 1;
    	} else {
              endOfValues = text.find(END_PARENTHESIS, pos);
    		  while(pos < endOfValues) {
                    leftLim = text.find_first_not_of(SPACE_STR, pos);
                     nextSpace = text.find(SPACE_STR, leftLim);
    				   	if (nextSpace < endOfValues) {
    				    	   rightLim = nextSpace;
    				       } else {
    				    	    rightLim = endOfValues;
    				       }
    				   len = rightLim - leftLim;
    				   v = text.substr(leftLim, len);
                       //cout<<"v: "<<v<<endl;
    				   value.push_back(v);
    				   pos = rightLim + 1;
    				   leftLim = pos;
    				    	 }
    				    	///a check for debugging:
                            //cout << "value contains:";
                              for (it=value.begin(); it<value.end(); it++) {
                                std::cout << ' ' << *it<<SPACE_STR;
    				    	  cout<<endl;
}
    				 //adds the new key and value to the list
    				    	toReturn.insert (std::pair<string,vector<string> >(key, value));
    				//clears the key and the value
    				    	value.clear();
    				    	key.clear();
    				    	pos = rightLim + 1;
    				    	}} else {
    				    		break;
    				    	}
    				    }
                        //cout<<"parsed alright"<<endl;
    				    return toReturn;
 }

/*************************************************************************
* function name:changeStrategy*
* The Input:bool*
* The output:           *
* The Function operation:changes the strategy*
*************************************************************************/
void DataAnalyzer::changeStrategy(bool offense) {
	this->brain->changeStrategy(offense);
}

/**************************************************************
* function name: parseHear        *
* The Input: const string text             *
* The output: map<string, vector <string>>             *
* The Function operation: gets a parseHear message and
* parses it into a map data-structure           *
***************************************************************/
vector<string> DataAnalyzer::parseHear(const string text) {
    std::vector<string> toReturn;
    std::vector<string>::iterator it;
//where we are on the string:
    unsigned int pos = 2;
//2 strings in which the data will be put
    string v, degrees, side, message, id;
//will be used to say which substring to take now
    unsigned int leftLim, rightLim, len;
//runs all over the string:
    //while (pos < text.length()) {
//finds first space - after "hear"
        leftLim = text.find(SPACE_STR, pos) + 1;
        pos = leftLim;
//finds the second space- appears after 'time'
        leftLim = text.find(SPACE_STR, pos) + 1;
        rightLim = text.find(SPACE_STR, leftLim);
        len = rightLim - leftLim;
        v = text.substr(leftLim, len);
 //checks who the speaker is
        if (v == "referee") {
    //we pull out the message, push it to the vector and return
            pos = rightLim + 1;
            leftLim = pos;
            rightLim = text.find(END_PARENTHESIS, pos);
            len = rightLim - leftLim;
            message = text.substr(leftLim, len);
            toReturn.push_back(message);
        } else {
    //we check if the speaker is from our team:
            pos = rightLim + 1;
            leftLim = pos;
            rightLim = text.find(SPACE_STR, pos);
            len = rightLim - leftLim;
            side = text.substr(leftLim, len);
    //if not- we push not to the vector and return
            if (side != "our") {
                v.clear();
                v = "not";
                toReturn.push_back(v);
                v.clear();
                v= "our";
                toReturn.push_back(v);
            } else {
   //we push the  degrees to the vector:
                toReturn.push_back(v);
   //then we find the id and push it to the vector
                ///(hear 0 88 our 1 "yoohoo")
                pos = rightLim + 1 ;
                leftLim = rightLim + 1;
                //pos = leftLim;
                rightLim = text.find(SPACE_STR, pos);
                len = rightLim - leftLim;
                id = text.substr(leftLim, len);
                toReturn.push_back(id);
   //and we get the message, push it to the vector and return
                pos = rightLim ;
                leftLim = text.find(MERCHAOT, pos) + 1;
                rightLim = text.find(MERCHAOT, leftLim) ;
                len = rightLim - leftLim;
                message = text.substr(leftLim, len);
                toReturn.push_back(message);
            }
        }
      return toReturn;
}


/*************************************************************************
* function name:parseOpeningReferee*
* The Input:string*
* The output:string*
* The Function operation:finds the referee's opening msg*
*************************************************************************/
string DataAnalyzer::parseOpeningReferee(const string text)
{
    unsigned int spaceBefore = text.find_last_of(SPACE);
    unsigned int spaceAfter = text.find_last_of(END_PARENTHESIS, spaceBefore);

    string refereeMsg = text.substr(spaceBefore + 1, spaceAfter - spaceBefore - 1);

    return refereeMsg;
}

/*************************************************************************
* function name:parsePlayerNum*
* The Input:string*
* The output:string*
* The Function operation:finds the player's number*
*************************************************************************/
string DataAnalyzer::parsePlayerNum(const string text)
{
    unsigned int spaceBefore = text.find_first_of(SPACE);
    spaceBefore = text.find_first_of(SPACE, spaceBefore+1);
    unsigned int spaceAfter = text.find_first_of(SPACE, spaceBefore + 1);

    string playerNum = text.substr(spaceBefore + 1, spaceAfter - spaceBefore - 1);

    return playerNum;
}


