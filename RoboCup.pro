TEMPLATE = app
CONFIG += thread
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    brainfactory.cpp \
    coach.cpp \
    dataanalyzer.cpp \
    defender.cpp \
    goalkeeper.cpp \
    midfielder.cpp \
    network.cpp \
    optionmaker.cpp \
    player.cpp \
    striker.cpp \
    tcp.cpp \
    udp.cpp \
    brain.cpp

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    brainfactory.h \
    coach.h \
    dataanalyzer.h \
    defender.h \
    goalkeeper.h \
    midfielder.h \
    network.h \
    optionmaker.h \
    player.h \
    striker.h \
    tcp.h \
    udp.h \
    brain.h

