/****************************************
* Student Name:Yonatan Azaria*
* Exercise Name:Multi-Threading*
* File description:TCP server and client*
****************************************/
#ifndef TCP_H
#define TCP_H
#include "network.h"

#include <iostream>
#include "string.h"

#include <iostream>

#include <sys/socket.h>

#include <netinet/in.h>


#include <arpa/inet.h>

#include <unistd.h>


using namespace std;

class TCP: public Network
{
private:
    //socket which communication will go through
    int sock;
    //socket for binding server to client
    int listening_sock;
    //port number
    int port;
    //bool to check if object is server
    bool isServer;
    /*************************************************************************
    * function name:makeContact*
    * The Input:              *
    * The output:              *
    * The Function operation:tries to connect to TCP server*
    *************************************************************************/
    void makeContact();
    /*************************************************************************
    * function name:getContact*
    * The Input:              *
    * The output:              *
    * The Function operation:accepts connection from TCP client*
    *************************************************************************/
    void getContact();
public:
    /*************************************************************************
    * function name:TCP*
    * The Input:port number and bool if server*
    * The output:TCP object*
    * The Function operation:makes a TCP server/client*
    *************************************************************************/
    TCP(int portNum, bool server = false);
    /*************************************************************************
    * function name:~TCP*
    * The Input:              *
    * The output:              *
    * The Function operation:default destructor*
    *************************************************************************/
    ~TCP();
    /*************************************************************************
    * function name:deliver*
    * The Input:receives data in char[] format and size of data*
    * The output:              *
    * The Function operation:sends the data to the server/client*
    *************************************************************************/
    void deliver(const char sendData[], int dataLength);
    /*************************************************************************
    * function name:receive*
    * The Input:              *
    * The output:returns received data in string format*
    * The Function operation:returnes data from the server/client*
    *************************************************************************/
    string receive();
    /*************************************************************************
    * function name:finish*
    * The Input:              *
    * The output:              *
    * The Function operation:closes the socket*
    *************************************************************************/
    void finish();
};

#endif // TCP_H
