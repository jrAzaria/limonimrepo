#ifndef GOALKEEPER_H
#define GOALKEEPER_H
#include "brain.h"


class GoalKeeper : public Brain
{
private:
    bool catched;
private:
    /*************************************************************************
    * function name:catchBall*
    * The Input:string*
    * The output:           *
    * The Function operation:catches a ball*
    *************************************************************************/
    void catchBall(string direction);
public:
    /*************************************************************************
    * function name:constructor*
    * The Input:int int int int string string string string*
    * The output:           *
    * The Function operation:makes a brain for a goal keeper and make bounds, x & y for move, sets defense and offense points*
    *************************************************************************/
    GoalKeeper(pair<double, double> infX, pair<double, double> supX, pair<double, double> infY, pair<double, double> supY, string moveX, string moveY, pair<string, string> defense, pair<string,string> offense);
    /*************************************************************************
    * function name:decideAction*
    * The Input:            *
    * The output:           *
    * The Function operation:decides what's the best next move*
    *************************************************************************/
    void decideAction();

};

#endif // GOALKEEPER_H
