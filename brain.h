/****************************************
* Student Name:Yonatan Azaria & Meriav Ben-Itzhak*
* Exercise Name:RoboCupTournament*
* File description:A brain for deciding what the player should do*
****************************************/
#ifndef BRAIN_H
#define BRAIN_H
#include <string>
#include <map>
#include "player.h"
#include "optionmaker.h"

#define TO_POST "toPost"
#define STEAL_BALL "stealBall"
#define PASS_TO "passTo"
#define TRY_SCORE "tryScore"
#define NO_ONE " "
#define GOT_BALL "gotBall"
#define SELF "self"
#define HEAR_PLAYER_ANGLE 0
#define HEAR_PLAYER_NUM 1
#define HEAR_PLAYER_MSG 2
#define PLAYER_MSG_SIZE 3
#define REFEREE_MSG_SIZE 1

#define RIGHT 'r'
#define LEFT 'l'

#define NO_MOTIVATION 0
#define LOW_MOTIVATION 10
#define L_MED_MOTIVATION 20
#define MED_MOTIVATION 30
#define M_HIGH_MOTIVATION 40
#define HIGH_MOTIVATION 50

#define NUM_OF_PARAM 7
enum {tmMtNrBll, rvlNearBll, seeGl, tmMtNr, bllKck, bllNr, otOfBnds};
#define NUM_OF_BOUNDS 4
enum {minX, maxX, minY, maxY};



class Brain
{
protected:
    //messages for player, side dependent
    string kickOffUs;
    string kickOffThem;
    string kickInUs;
    string kickInThem;
    string freeKickUs;
    string freeKickThem;
    string cornerKickUs;
    string cornerKickThem;
    string goalKickUs;
    string goalKickThem;
    string goalUs;
    string goalThem;
    string offsideUs;
    string offsideThem;

    pair<int,int> infXLeftRight;
    pair<int, int> supXLeftRight;
    pair<int, int> infYLeftRight;
    pair<int, int> supYLeftRight;
    pair<string, string> defenseLeftRight;
    pair<string,string> offenseLeftRight;

    Player * player;
    char side;
    char otherSide;
    string playerNum;
    string moveCommand;

    vector<string> myParameters;
    vector<double> bounds;
    bool offenseStrategy;

    map<string, vector<string> >seeData;
    bool lockSeeData;
    bool newSeeData;

    map<string, vector<string> > senseData;
    bool lockSenseData;
    bool newSenseData;

    vector<string> hearData;
    bool lockHearData;
    bool newHearData;

    double ballDistParam;

    string defenseLocation;
    string offenseLocation;

    OptionMaker * options;

    string myGoal;
    string theirGoal;

    bool kickable;

    map<string, vector<int> > functions;

    /*************************************************************************
    * function name:run*
    * The Input:float distance*
    * The output:           *
    * The Function operation:dashes according to how far the tagert is*
    *************************************************************************/
    void run(float distance);
    /*************************************************************************
    * function name:toPost*
    * The Input:            *
    * The output:           *
    * The Function operation:goes to post, depending on current strategy*
    *************************************************************************/
    void toPost();
    /*************************************************************************
    * function name:stealBall*
    * The Input:string*
    * The output:           *
    * The Function operation:steals the ball from a destination*
    *************************************************************************/
    void stealBall();
    /*************************************************************************
    * function name:stealBall*
    * The Input:pair<float,int>*
    * The output:           *
    * The Function operation:steals the ball from a destination*
    *************************************************************************/
    void stealBall(pair<float, string> ballLocation);
    /*************************************************************************
    * function name:goTowards*
    * The Input:string*
    * The output:           *
    * The Function operation:goes towards give destination*
    *************************************************************************/
    void goTowards(const string destination);
    /*************************************************************************
    * function name:pass*
    * The Input:int, string*
    * The output:               *
    * The Function operation:passes the ball to a target using the distance and direction*
    *************************************************************************/
    void pass(float distance, string direction);
    /*************************************************************************
    * function name:passTo*
    * The Input:string*
    * The output:               *
    * The Function operation:passes the ball to given team mate*
    *************************************************************************/
    void passTo(const string teamMate);
    /*************************************************************************
    * function name:passTo*
    * The Input:pair<unsigned int, int>*
    * The output:               *
    * The Function operation:passes the ball to given location*
    *************************************************************************/
    void passTo(const pair <unsigned int, string> teamMate, string name = NO_ONE);
    /*************************************************************************
    * function name:findBall*
    * The Input:            *
    * The output:           *
    * The Function operation:looks for the ball*
    *************************************************************************/
    void findBall();
    /*************************************************************************
    * function name:tryScore*
    * The Input:            *
    * The output:           *
    * The Function operation:tries to score*
    *************************************************************************/
    void tryScore();
    /*************************************************************************
    * function name:tryScore*
    * The Input:pair <unsigned int, int>*
    * The output:           *
    * The Function operation:tries to score using a goal location*
    *************************************************************************/
    void tryScore(pair<unsigned int, string> goalLocation);
    /*************************************************************************
    * function name:tryScore*
    * The Input:pair <float, int>*
    * The output:           *
    * The Function operation:tries to score using the ball's location*
    *************************************************************************/
    void tryScore(pair <float, string> ballLocation);
    /*************************************************************************
    * function name:handleHearMsg*
    * The Input:            *
    * The output:           *
    * The Function operation:handles hear messages*
    *************************************************************************/
    void handleHearMsg();

public:
    Brain(){}
    /*************************************************************************
    * function name:constructor*
    * The Input:int int int int string string string string*
    * The output:           *
    * The Function operation:makes a brain for a player and make bounds, x & y for move, sets defense and offense points*
    *************************************************************************/
    Brain(pair<double,double> infX, pair<double,double> supX, pair<double,double> infY, pair<double,double> supY, string moveX, string moveY, pair<string, string> defense, pair<string,string> offense);
    /*************************************************************************
    * function name:destructor*
    * The Input:            *
    * The output:           *
    * The Function operation:destructs all objects in brain*
    *************************************************************************/
    virtual ~Brain();
    /*************************************************************************
    * function name:setPlayer*
    * The Input:vector<string>*
    * The output:           *
    * The Function operation:sets the player's parameters, his client, the sides and his number*
    *************************************************************************/
    void setPlayer(vector<string> playerParameters, UDP *client, char mySide, char hisSide, string playerNumber);
    /*************************************************************************
    * function name:getSee*
    * The Input:map<string, vector<string> >*
    * The output:           *
    * The Function operation:gets see data parameters*
    *************************************************************************/
    void getSee(map<string, vector<string> > seeParameters);
    /*************************************************************************
    * function name:getSense*
    * The Input:map<string, vector<string> >*
    * The output:           *
    * The Function operation:gets sense data parameters*
    *************************************************************************/
    void getSense(map<string, vector<string> > senseParameters);
    /*************************************************************************
    * function name:gethear*
    * The Input:vector<string>*
    * The output:           *
    * The Function operation:gets hear data parameters*
    *************************************************************************/
    void getHear(vector<string> hearParameters);
    /*************************************************************************
    * function name:changeStrategy*
    * The Input:bool*
    * The output:           *
    * The Function operation:changes the strategy*
    *************************************************************************/
    void changeStrategy(bool offense);
    /*************************************************************************
    * function name:decideAction*
    * The Input:            *
    * The output:           *
    * The Function operation:decides what's the best next move*
    *************************************************************************/
    virtual void decideAction();

};

#endif // BRAIN_H
