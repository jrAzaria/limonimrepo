#include "dataanalyzer.h"
#include "coach.h"
#include <iostream>
#include <stdlib.h>

#include "brain.h"
#include "brainfactory.h"


#define PI 3.14159265
#define TRIANGLE 180
#include <math.h>

using namespace std;

#include <string>

#define OTHER_TEAM_STR "otherTeam"
#define LIMONIM_STR "limonim"
#define NUM_OF_PLAYERS 10
#define DIFF_PLAYERS 4
#define POS_PLAYERS 4

void * playGoalie(void * playGame)
{
    DataAnalyzer * obj = (DataAnalyzer*)(playGame);
    obj->play(true);

    pthread_exit(NULL);
}

void * playPlayer(void * playGame)
{
    DataAnalyzer * obj = (DataAnalyzer*)(playGame);
    obj->play();

    pthread_exit(NULL);
}

int main()
{
//    unsigned int i=0, j=0;
//    BrainFactory * factory = new BrainFactory();

//    Brain * goalie = factory->createBrain(i, j);

//    DataAnalyzer * goalieData = new DataAnalyzer(goalie);

//    ++i;

//    vector<DataAnalyzer *> allPlayers;

//    for (i=1; i< DIFF_PLAYERS; ++i)
//    {
//        for (j=0; j < POS_PLAYERS; ++j)
//        {
//            Brain * newPlayer = factory->createBrain(i, j);
//            if (newPlayer != NULL)
//            {
//                DataAnalyzer * newPlayerData = new DataAnalyzer(newPlayer);
//                allPlayers.push_back(newPlayerData);
//            }
//        }
//    }

////    Brain * newPlayer = factory->createBrain(2, 0);
////    if (newPlayer != NULL)
////    {
////        DataAnalyzer * newPlayerData = new DataAnalyzer(newPlayer);
////        allPlayers.push_back(newPlayerData);
////    }

////    newPlayer = factory->createBrain(3, 0);
////    if (newPlayer != NULL)
////    {
////        DataAnalyzer * newPlayerData = new DataAnalyzer(newPlayer);
////        allPlayers.push_back(newPlayerData);
////    }

//    pthread_t newThreads[allPlayers.size()];



//    for (i=0; i < allPlayers.size(); ++i)
//        pthread_create(&newThreads[i], NULL, &playPlayer, allPlayers[i]);

//    pthread_create(&newThreads[i], NULL, &playGoalie, goalieData);

//    for (i=0; i<= allPlayers.size(); ++i)
//        pthread_join(newThreads[i], NULL);

//    //clear memory
//    if (factory != NULL)
//        delete factory;

//    if (goalieData!=NULL)
//        delete goalieData;


//    for (i=0; i < allPlayers.size(); ++i)
//    {
//        if (allPlayers[i]!=NULL)
//            delete allPlayers[i];
//    }

//    pthread_exit(NULL);


    BrainFactory * factory = new BrainFactory();

     Brain * me = factory->createBrain(2, 0);

     DataAnalyzer * meDa = new DataAnalyzer(me);

     string text = "(hear 0 88 our 1 \"yoohoo\")";

     std::vector<string> toReturn = meDa->parseHear(text);

delete factory;
     delete me;
     delete meDa;

    return 0;

}

