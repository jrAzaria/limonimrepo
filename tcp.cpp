/****************************************
* Student Name:Yonatan Azaria*
* Exercise Name:Multi-Threading*
* File description:TCP server and client*
****************************************/
#include "tcp.h"


/*************************************************************************
* function name:TCP*
* The Input:port number and bool if server*
* The output:TCP object*
* The Function operation:makes a TCP server/client and defining an appropiate socket to listen or send to*
*************************************************************************/
TCP::TCP(int portNum, bool server)
{
    this->isServer = server;

    //if object is server
    if (server)
    {
        //set portnum and listening socket
        this->port = portNum;
        this->listening_sock = socket(AF_INET, SOCK_STREAM, 0);
        if (this->listening_sock < 0) {
            cout << "server couldn't set socket\n";// error
        }
        else
        {
            //set an endpoint address to connect to a listening socket
            struct sockaddr_in primarySockAddress;
            memset(&primarySockAddress, 0, sizeof(primarySockAddress));
            primarySockAddress.sin_family = AF_INET;
            primarySockAddress.sin_addr.s_addr = INADDR_ANY;
            primarySockAddress.sin_port = htons(this->port);
            //bind to socket
            if (bind(this->listening_sock, (struct sockaddr *) &primarySockAddress, sizeof(primarySockAddress)) < 0) {
                cout<<"couldn't bind socket\n";//error
            }
            else
            {
                int backLog = 0;
                //check for errors
                if (listen(this->listening_sock, backLog) < 0) {
                    cout<<"server couldn't connect to socket, received log :"<< backLog << "\n";
                }
                //try to connect to a client
                else
                    this->getContact();
            }
        }
    }
    //if object is client
    else
    {
        //set port and socket for communication
        this->port = portNum;
        this->sock = socket(AF_INET, SOCK_STREAM, 0);
        if (this->sock < 0) {
        cout<<"client couldn't set socket\n";// error
        }
        //try to connect to a server
        else
            this->makeContact();
    }
}
/*************************************************************************
* function name:~TCP*
* The Input:              *
* The output:              *
* The Function operation:default destructor*
*************************************************************************/
TCP::~TCP()
{

}
/*************************************************************************
* function name:makeContact*
* The Input:              *
* The output:              *
* The Function operation:tries to connect to TCP server using the socket from the constructor*
*************************************************************************/
void TCP::makeContact()
{
    //define the access point
    struct sockaddr_in primarySockAddress;
    memset(&primarySockAddress, 0, sizeof(primarySockAddress));
    primarySockAddress.sin_family = AF_INET;
    primarySockAddress.sin_addr.s_addr = inet_addr(IP_ADDRESS);
    primarySockAddress.sin_port = htons(this->port);
    //try to connect
    if (connect(this->sock, (struct sockaddr *) &primarySockAddress, sizeof(primarySockAddress)) < 0) {
    cout<<"client couldn't make contact\n";// error
    }
    //else
      //  cout<<"client made contact\n";
}
/*************************************************************************
* function name:getContact*
* The Input:              *
* The output:              *
* The Function operation:accepts connection from TCP client using the listening socket and defines a socket to communicate through*
*************************************************************************/
void TCP::getContact()
{
    //endpoint address of client's socket
    struct sockaddr_in clientAddress;
    unsigned int clientAddressLen = sizeof(clientAddress);
    //accept connection from client
    this->sock = accept(this->listening_sock, (struct sockaddr *) &clientAddress, &clientAddressLen);
    if (this->sock < 0) {
        cout<<"server couldn't make contact\n";// error
    }
    //else
      //  cout<<"server made contact\n";
}
/*************************************************************************
* function name:deliver*
* The Input:receives data in char[] format and size of data*
* The output:              *
* The Function operation:sends the data to the server/client using previosly received port number, socket and connection*
*************************************************************************/
void TCP::deliver(const char sendData[], int dataLength)
{
    //send data to client/server
    int sent_bytes = send(this->sock, sendData, dataLength, 0);
    //check for errors
    if (sent_bytes < 0) {
        cout<<"send error\n";
    }
    //else
      //  cout<<"sent: "<< sendData <<"\n";

}
/*************************************************************************
* function name:receive*
* The Input:              *
* The output:returns received data in string format*
* The Function operation:returnes data from the server/client using previosly received port number, socket and connection*
*************************************************************************/
string TCP::receive()
{
    //make a buffer for info and init it
    char buffer[BUFFER_MAX];
    for (int i=0; i<(int)sizeof(buffer); ++i)
        buffer[i] = '\0';
    int estDataLength = sizeof(buffer);
    //receive info
    int bytesRead = recv(this->sock, buffer, estDataLength, 0);
    //check for errors
    if (bytesRead == 0) {
        cout<<"connection closed\n";
    }
    else if (bytesRead < 0) {
        cout<<"receive failed\n";
    }
    else
        cout<<buffer <<"\n";

    //make buffer into string and return
    string returnString(buffer);
    return returnString;

}
/*************************************************************************
* function name:finish*
* The Input:              *
* The output:              *
* The Function operation:closes the socket using close() function*
*************************************************************************/
void TCP::finish()
{
   close(this->sock);
   //if server close also the listening socket
   if (this->isServer)
       close(this->listening_sock);
}























