/****************************************
* Student Name:Yonatan Azaria*
* Exercise Name:Multi-Threading*
* File description:Network - an interface for both UDP and TCP*
****************************************/
#ifndef NETWORK_H
#define NETWORK_H
#include <iostream>
#include "string.h"

#include <iostream>

#include <sys/socket.h>

#include <netinet/in.h>


#include <arpa/inet.h>

#include <unistd.h>

#define BUFFER_MAX 4096
#define IP_ADDRESS "127.0.0.1"
using namespace std;

class Network
{
public:
    /*************************************************************************
    * function name:Network*
    * The Input:              *
    * The output:Network object*
    * The Function operation:default constructor*
    *************************************************************************/
    Network();
    /*************************************************************************
    * function name:~Network*
    * The Input:              *
    * The output:              *
    * The Function operation:default destructor*
    *************************************************************************/
    virtual ~Network() {}
    /*************************************************************************
    * function name:deliver*
    * The Input:receives data in char[] format and size of data*
    * The output:              *
    * The Function operation:sends the data to the server/client*
    *************************************************************************/
    virtual void deliver(const char sendData[], int dataLength) = 0;
    /*************************************************************************
    * function name:receive*
    * The Input:              *
    * The output:returns received data in string format*
    * The Function operation:returnes data from the server/client*
    *************************************************************************/
    virtual string receive() = 0;
    /*************************************************************************
    * function name:finish*
    * The Input:              *
    * The output:              *
    * The Function operation:closes the socket*
    *************************************************************************/
    virtual void finish() = 0;
};

#endif // NETWORK_H
