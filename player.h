/****************************************
* Student Name:Yonatan Azaria*
* Exercise Name:Multi-Threading*
* File description:Player - a player on the field that can do anything he's told to*
****************************************/
#ifndef PLAYER_H
#define PLAYER_H
#include "udp.h"
#include <utility>

class Player
{
private:
    char side;
    UDP * client;
    std::pair <const char *, const char * > kick;
public:
    /*************************************************************************
    * function name:Player*
    * The Input:              *
    * The output:object player*
    * The Function operation:consructs a player object containing his sideand a client*
    *************************************************************************/
    Player(UDP * playerClient, char playerSide);
    /*************************************************************************
    * function name:Player*
    * The Input:              *
    * The output:object player*
    * The Function operation:consructs a player object containing his side, client and location*
    *************************************************************************/
    Player(UDP * playerClient, char playerSide, const char * x, const char * y);
    /*************************************************************************
    * function name:operator+*
    * The Input:const char power to dash*
    * The output:           *
    * The Function operation:dashes with inputed power*
    *************************************************************************/
    void operator +(const char * power);
    /*************************************************************************
    * function name:operator-*
    * The Input:const char power to dash*
    * The output:           *
    * The Function operation:dashes backwards with inputed power*
    *************************************************************************/
    void operator -(const char *power);
    /*************************************************************************
    * function name:operator<*
    * The Input:const char angles CCW*
    * The output:           *
    * The Function operation:turns CCW by inputed angles*
    *************************************************************************/
    void operator < (const char *angle);
    /*************************************************************************
    * function name:operator>*
    * The Input:const char angles CW*
    * The output:           *
    * The Function operation:turns CW by inputed angles*
    *************************************************************************/
    void operator > (const char *angle);    
    /*************************************************************************
    * function name:operator<=*
    * The Input:const char power*
    * The output:*this so it can be connected to >= (kick direction)*
    * The Function operation:gets kick power*
    *************************************************************************/
    Player &operator <=(const char *power);
    /*************************************************************************
    * function name:operator>=*
    * The Input:const char power*
    * The output:       *
    * The Function operation:gets kick direction*
    *************************************************************************/
    void operator >= (const char *direction);
    /*************************************************************************
    * function name:say*
    * The Input:const char power*
    * The output:       *
    * The Function operation:says a message*
    *************************************************************************/
    void say(const char *message);
    /*************************************************************************
    * function name:checkScore*
    * The Input:            *
    * The output:       *
    * The Function operation:checks the score*
    *************************************************************************/
    void checkScore();
    /*************************************************************************
    * function name:checkScore*
    * The Input:            *
    * The output:       *
    * The Function operation:checks the score*
    *************************************************************************/
    void catchBall(const char * direction);

};

#endif // PLAYER_H
