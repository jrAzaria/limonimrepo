#ifndef OPTIONMAKER_H
#define OPTIONMAKER_H
#include <vector>
#include <map>
#include <string>

using namespace std;

class OptionMaker
{
private:
    char side;
    char otherSide;
    string myGoal;
    string rivalFlag;
    string ourFlag;

    vector<string> parameters;
    map<string , vector<int> > functions;

    vector<string> trianFlags;
    pair<double, double> playerCoords;
    bool playerCoordsFound;

    map<string, vector<string> > seeData;
    map<string, vector<string> > senseData;

    vector<double> bounds;

    std::pair<unsigned int, string>  closestTeamMate;
    bool teamMateLocation;
    string closestTeamMateNum;
    bool closestTeamMateNumFound;

    bool towardsRival;

    int playerDirection;

    bool rivalNextToBall;

    bool teamMateNextToBall;

    std::pair <float, string> ball;
    bool ballLocation;

    bool kickable;

    std::pair <unsigned int , string> goal;
    bool goalLocation;
    /*************************************************************************
    * function name:parsePlayerNum*
    * The Input: string playerName*
    * The output:                       *
    * The Function operation:parses the player's number*
    *************************************************************************/
    string parsePlayerNum(string playerName);
    /*************************************************************************
    * function name:parseplayers*
    * The Input:                        *
    * The output:                       *
    * The Function operation:parses the players, checks teammates locations and if any player is next to the ball*
    *************************************************************************/
    void parsePlayers();
    /*************************************************************************
    * function name:parseBall*
    * The Input:                        *
    * The output:                       *
    * The Function operation:parses the ball from see data*
    *************************************************************************/
    void parseBall();
    /*************************************************************************
    * function name:parseGoal*
    * The Input:                        *
    * The output:                       *
    * The Function operation:parses the goal from see data*
    *************************************************************************/
    void parseGoal();
    /*************************************************************************
    * function name:parseDirection*
    * The Input:                        *
    * The output:                       *
    * The Function operation:parses the direction of the player*
    *************************************************************************/
    void parseDirection();
    /*************************************************************************
    * function name:calculateLimit*
    * The Input:                        *
    * The output:                       *
    * The Function operation:calculates the limiting factor and the results*
    *************************************************************************/
    int calculateLimit(float value, float distance);
    /*************************************************************************
    * function name:calculatePrice*
    * The Input: vector<int>    *
    * The output:                       *
    * The Function operation:calculates the price for each functions*
    *************************************************************************/
    int calculatePrice(vector<int> vars);
    /*************************************************************************
    * function name:parseLimit*
    * The Input:            *
    * The output:           *
    * The Function operation:gets the player's location and limits*
    *************************************************************************/
    void parseLimits();
    /*************************************************************************
    * function name:triangulate*
    * The Input:            *
    * The output: pair coordinates*
    * The Function operation:gets the player's location*
    *************************************************************************/
    pair<double, double> triangulate(double distA, double distB, double angleC, string flagA, string flagB);
    /*************************************************************************
    * function name:nextToBall*
    * The Input:            *
    * The output:           *
    * The Function operation:checks if a player is close to a ball*
    *************************************************************************/
    bool nextToBall(double distBall, double distPlayer, double angleBetween);
public:
    /*************************************************************************
    * function name:constructor*
    * The Input:vector<string>, char, char*
    * The output:                       *
    * The Function operation:constructs an optins maker base on the brain's parameters and functions*
    *************************************************************************/
    OptionMaker(vector<string> brainParameters, map<string, vector<int> > functionParameters ,char ourSide, char theirSide);
    /*************************************************************************
    * function name:getOptions*
    * The Input:vector<vector<string>>*
    * The output:std::map <void*, int>*
    * The Function operation:gets all possible options, their variable and their price(time) and puts in a vector*
    *************************************************************************/
    string getOptions(map<string, vector<string> > see, map<string, vector<string> > sense, vector<double> playerLimits, bool ballKickable);
    /*************************************************************************
    * function name:getBallLocation*
    * The Input:                *
    * The output:pair <float, int>*
    * The Function operation:returns the location of ball*
    *************************************************************************/
    pair<float, string> getBallLocation();
    /*************************************************************************
    * function name:getTeamMateLocation*
    * The Input:                *
    * The output:pair <float, int>*
    * The Function operation:returns the location of closest team mate*
    *************************************************************************/
    pair<unsigned int, string> getTeamMateLocation();
    /*************************************************************************
    * function name:getTeamMateNumber*
    * The Input:                *
    * The output:string*
    * The Function operation:returns the number of the closest team mate*
    *************************************************************************/
    string getTeamMateNumber();
    /*************************************************************************
    * function name:getGoalLocation*
    * The Input:                *
    * The output:pair <float, int>*
    * The Function operation:returns the location of the goal*
    *************************************************************************/
    pair<unsigned int, string> getGoalLocation();
    /*************************************************************************
    * function name:checkGoalfound*
    * The Input:                *
    * The output:bool*
    * The Function operation:returns if the location of the goal was found*
    *************************************************************************/
    bool checkGoalfound();
    /*************************************************************************
    * function name:checkBallFound*
    * The Input:                *
    * The output:bool*
    * The Function operation:returns if the location of ball was found*
    *************************************************************************/
    bool checkBallFound();
    /*************************************************************************
    * function name:checkTeamMateFound*
    * The Input:                *
    * The output:bool*
    * The Function operation:returns if the location of closest team mate was found*
    *************************************************************************/
    bool checkTeamMateFound();
    /*************************************************************************
    * function name:checkTeamMateNumberFound*
    * The Input:                *
    * The output:bool*
    * The Function operation:returns if the number of the closest team mate was found*
    *************************************************************************/
    bool checkTeamMateNumberFound();

};

#endif // OPTIONMAKER_H
