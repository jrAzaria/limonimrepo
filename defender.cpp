#include "defender.h"

/*************************************************************************
* function name:constructor*
* The Input:int int int int string string string string*
* The output:           *
* The Function operation:makes a brain for a defender and make bounds, sets x & y coords for move
*  sets defense and offense points*
*************************************************************************/
Defender::Defender(pair<double, double> infX, pair<double, double> supX, pair<double, double> infY, pair<double, double> supY, string moveX, string moveY, pair<string, string> defense, pair<string,string> offense): Brain(infX, supX, infY, supY, moveX, moveY, defense, offense)
{
    //setting parameters for each function,
    vector<int> functionCost(NUM_OF_PARAM);
    functionCost[tmMtNrBll] = HIGH_MOTIVATION;
    functionCost[rvlNearBll] = NO_MOTIVATION;
    functionCost[seeGl] = LOW_MOTIVATION;
    functionCost[tmMtNr] = NO_MOTIVATION;
    functionCost[bllKck] = NO_MOTIVATION;
    functionCost[bllNr] = NO_MOTIVATION;
    functionCost[otOfBnds] = HIGH_MOTIVATION;
    this->functions[TO_POST] = functionCost;

    functionCost[tmMtNrBll] = NO_MOTIVATION;
    functionCost[rvlNearBll] = HIGH_MOTIVATION;
    functionCost[seeGl] = LOW_MOTIVATION;
    functionCost[tmMtNr] = NO_MOTIVATION;
    functionCost[bllKck] = NO_MOTIVATION;
    functionCost[bllNr] = MED_MOTIVATION;
    functionCost[otOfBnds] = -LOW_MOTIVATION;
    this->functions[STEAL_BALL] = functionCost;

    functionCost[tmMtNrBll] = NO_MOTIVATION;
    functionCost[rvlNearBll] = HIGH_MOTIVATION;
    functionCost[seeGl] = NO_MOTIVATION;
    functionCost[tmMtNr] = HIGH_MOTIVATION;
    functionCost[bllKck] = HIGH_MOTIVATION;
    functionCost[bllNr] = LOW_MOTIVATION;
    functionCost[otOfBnds] = -LOW_MOTIVATION;
    this->functions[PASS_TO] = functionCost;

    functionCost[tmMtNrBll] = NO_MOTIVATION;
    functionCost[rvlNearBll] = L_MED_MOTIVATION;
    functionCost[seeGl] = MED_MOTIVATION;
    functionCost[tmMtNr] = NO_MOTIVATION;
    functionCost[bllKck] = MED_MOTIVATION;
    functionCost[bllNr] = LOW_MOTIVATION;
    functionCost[otOfBnds] = -LOW_MOTIVATION;
    this->functions[TRY_SCORE] = functionCost;

}
