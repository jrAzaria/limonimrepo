/****************************************
* Student Name:Yonatan Azaria & Meriav Ben-Itzhak*
* Exercise Name:RoboCupTournament*
* File description:A brain for deciding what the player should do*
****************************************/
#include "brain.h"
#include <stdlib.h>
#include <math.h>
#include <sstream>

#define BALL "b"
#define GOAL "g"
#define DISTANCE 0
#define DIRECTION 1
#define VIEW_ANGLE 15
#define SEARCH "45"
#define TEAM_NAME "limonim"
#define TEAM_MATE "p \"limonim\""
#define MOVE_COMMAND "(move "
#define END_PARENTHESIS ")"
#define SPACE " "
#define KICK_MARGIN 0.6
#define MAX_KICK "100"
#define SOFT "33"
#define MILD "66"
#define STRONG "100"
#define MAX_RUN 1.00

#define BALL_DECAY 0.94
#define EXPO 200
#define KICK_POWER_RATE  0.027
#define AVG_NOISE 0.75

#define PASS "pass"

//messages
#define BEFORE_KICK_OFF "before_kick_off"
#define PLAY_ON "play_on"
#define DROP_BALL "drop_ball"
#define TIME_OVER "time_over"
#define KICK_OFF "kick_off_"
#define KICK_IN "kick_in_"
#define FREE_KICK "free_kick_"
#define CORNER_KICK "corner_kick_"
#define GOAL_KICK "goal_kick_"
#define GOAL_SCORE "goal_"
#define OFFSIDE "offside_"

#define MAX_NUM_GET_DATA 2

/*************************************************************************
* function name:constructor*
* The Input:int int int int string string string string*
* The output:           *
* The Function operation:makes a brain for a player and make bounds, x & y for move, sets defense and offense points*
*************************************************************************/
Brain::Brain(pair<double, double> infX, pair<double, double> supX, pair<double, double> infY, pair<double, double> supY, string moveX, string moveY, pair<string, string> defense, pair<string,string> offense)
{
    moveCommand = MOVE_COMMAND;
    moveCommand += moveX;
    moveCommand += SPACE;
    moveCommand += moveY;
    moveCommand += END_PARENTHESIS;

    infXLeftRight = infX;
    supXLeftRight = supX;
    infYLeftRight = infY;
    supYLeftRight = supY;
    defenseLeftRight = defense;
    offenseLeftRight = offense;
}
/*************************************************************************
* function name:destructor*
* The Input:            *
* The output:           *
* The Function operation:destructs all objects in brain*
*************************************************************************/
Brain::~Brain()
{
    //if we have an option maker - delete it
    if (this->options != NULL)
        delete this->options;

    //if we have a player - delete it
    if (this->player != NULL)
        delete this->player;
}

/*************************************************************************
* function name:setPlayer*
* The Input:vector<string>*
* The output:           *
* The Function operation:sets the player's parameters, his client, the sides and his number*
*************************************************************************/
void Brain::setPlayer(vector<string> playerParameters, UDP *client, char mySide, char hisSide, string playerNumber)
{
    //set location for players according to side
    if (mySide == LEFT)
    {
        this->defenseLocation = defenseLeftRight.first;
        this->offenseLocation = offenseLeftRight.first;
    }
    else
    {
        this->defenseLocation = defenseLeftRight.second;
        this->offenseLocation = offenseLeftRight.second;
    }

    //set bounds for player according to side
    this->bounds = vector<double>(NUM_OF_BOUNDS);
    if (mySide == LEFT)
    {
        this->bounds[minX] = infXLeftRight.first;
        this->bounds[maxX] = supXLeftRight.first;
        this->bounds[minY] = infYLeftRight.first;
        this->bounds[maxY] = supYLeftRight.first;
    }
    else
    {
        this->bounds[minX] = infXLeftRight.second;
        this->bounds[maxX] = supXLeftRight.second;
        this->bounds[minY] = infYLeftRight.second;
        this->bounds[maxY] = supYLeftRight.second;
    }

    //set location for player
    client->deliver(moveCommand.c_str(), moveCommand.size());

    //init player
    this->player = new Player(client, mySide);

    //init sides and player parameters
    this->side = mySide;
    this->otherSide = hisSide;
    this->myParameters = playerParameters;
    this->playerNum = playerNumber;

    //unlock everything
    this->lockHearData = false;
    this->lockSeeData = false;
    this->lockSenseData = false;

    //no new data
    this->newHearData = false;
    this->newSeeData = false;
    this->newSenseData = false;

    //build goal posts
    this->myGoal = GOAL;
    this->myGoal += SPACE;
    this->myGoal += mySide;

    this->theirGoal = GOAL;
    this->theirGoal += SPACE;
    this->theirGoal += hisSide;

    //build referee messages
    kickOffUs = KICK_OFF;
    kickOffUs += side;
    kickOffThem = KICK_OFF;
    kickOffThem += otherSide;
    kickInUs = KICK_IN;
    kickInUs += side;
    kickInThem = KICK_IN;
    kickInThem += otherSide;
    freeKickUs = FREE_KICK;
    freeKickUs += side;
    freeKickThem = FREE_KICK;
    freeKickThem += otherSide;
    cornerKickUs = CORNER_KICK;
    cornerKickUs += side;
    cornerKickThem = CORNER_KICK;
    cornerKickThem += otherSide;
    goalKickUs = GOAL_KICK;
    goalKickUs = side;
    goalKickThem = GOAL_KICK;
    goalKickThem = otherSide;
    goalUs = GOAL_SCORE;
    goalUs += side;
    goalThem = GOAL_SCORE;
    goalThem += otherSide;
    offsideUs = OFFSIDE;
    offsideUs += side;
    offsideThem = OFFSIDE;
    offsideThem += otherSide;

    //can't kick ball
    this->kickable = false;

    //calculate parameter for calculating ball movement, part of geometric progression formula
    ballDistParam = 1 - (pow(BALL_DECAY, EXPO));
    ballDistParam /= 1 - BALL_DECAY;

    //make the option maker
    this->options = new OptionMaker(this->myParameters, this->functions, this->side, this->otherSide);

}
/*************************************************************************
* function name:getSee*
* The Input:map<string, vector<string> >*
* The output:           *
* The Function operation:gets see data parameters*
*************************************************************************/
void Brain::getSee(map<string, vector<string> > seeParameters)
{
    //if data isn't locked
    if (!this->lockSeeData)
    {
        //insert data
        this->seeData = seeParameters;

        //indicate it's new
        this->newSeeData = true;

        //unlock it
        this->lockSeeData = true;
    }
    else
        cout <<"filtered\n";////////////////////////////////////debugging
}

/*************************************************************************
* function name:getSense*
* The Input:map<string, vector<string> >*
* The output:           *
* The Function operation:gets sense data parameters*
*************************************************************************/
void Brain::getSense(map<string, vector<string> > senseParameters)
{
    //if sense isn't locked
    if (!this->lockSenseData)
    {
        //insert data
        this->senseData = senseParameters;

        //indicate it's new
        this->newSenseData = true;

        //lock it
        this->lockSenseData = true;
    }
}

/*************************************************************************
* function name:gethear*
* The Input:vector<string>*
* The output:           *
* The Function operation:gets hear data parameters*
*************************************************************************/
void Brain::getHear(vector<string> hearParameters)
{
    //wait for hear data to unlock
    while (this->lockHearData);

    //lock it
    this->lockHearData = true;

    //insert data
    this->hearData = hearParameters;   

    //indicate that data is new
    this->newHearData = true;
}

/*************************************************************************
* function name:changeStrategy*
* The Input:bool*
* The output:           *
* The Function operation:changes the strategy*
*************************************************************************/
void Brain::changeStrategy(bool offense)
{
    this->offenseStrategy = offense;
}

//protected
/*************************************************************************
* function name:decideAction*
* The Input:            *
* The output:           *
* The Function operation:decides what's the best next move*
*************************************************************************/
void Brain::decideAction()
{
    //wait for data to be locked
    while (!this->lockSeeData || !this->lockSenseData)
    {
        //if a hear message was received
        if (newHearData)
        {
            //take care of it
            this->handleHearMsg();
        }

    }

    //get function to do
    string function = options->getOptions(this->seeData, this->senseData, this->bounds, this->kickable);

    //use the right function
    if(TO_POST == function)
        this->toPost();
    else
        if(STEAL_BALL == function)
        {
            if (options->checkBallFound())
                this->stealBall(options->getBallLocation());
            else
                this->stealBall();
        }
        else
            if(PASS_TO == function)
            {
                //if we found a team mate
                if (this->options->checkTeamMateFound())
                    //if we found his number - pass to his location and number
                    if (this->options->checkTeamMateNumberFound())
                        this->passTo(this->options->getTeamMateLocation(), this->options->getTeamMateNumber());
                    else
                        //pass to his location
                        this->passTo(this->options->getTeamMateLocation());
                else
                    this->passTo(NO_ONE);
            }
            else
                if(TRY_SCORE == function)
                {
                    //if we can kick the ball and we know where the goal is
                    if (this->kickable && this->options->checkGoalfound())
                    {
                        this->tryScore(this->options->getGoalLocation());
                    }
                    //if we can't kick the ball but know where it is
                    else
                        if (!this->kickable && this->options->checkBallFound())
                        {
                            this->tryScore(this->options->getBallLocation());
                        }
                    //if we can't help the function
                        else
                            this->tryScore();
                }

    //unlock data
    this->lockSeeData = false;
    this->lockSenseData = false;
}
/*************************************************************************
* function name:run*
* The Input:float distance*
* The output:           *
* The Function operation:dashes according to how far the tagert is*
*************************************************************************/
void Brain::run(float distance)
{
    //if the distance is larger than what the player can run in one dash - dash as hard as possible
    if (distance >= (float)MAX_RUN)
        *this->player + STRONG;
    else
    //if the distance is more than half of the power of one dash - dash mildly
    if (distance/MAX_RUN >= (float)MAX_RUN/2)
        *this->player + MILD;
    //if the distance is less than half the power of one dash - dash softly
    else
        *this->player + SOFT;
}

/*************************************************************************
* function name:toPost*
* The Input:            *
* The output:           *
* The Function operation:goes to post, depending on current strategy*
*************************************************************************/
void Brain::toPost()
{
    string currentLocation;
    if (this->offenseStrategy)
        currentLocation = this->offenseLocation;
    else
        currentLocation = this->defenseLocation;

    //go towards destination
    this->goTowards(currentLocation);
}

/*************************************************************************
* function name:stealBall*
* The Input:        *
* The output:           *
* The Function operation:steals the ball *
*************************************************************************/
void Brain::stealBall()
{
    //if we're not near the ball
    if (!this->kickable)
    {
        //if we can see the ball
        if (this->seeData.count(BALL) == 1)
        {
            //if ball is close enough mark it
            if (abs(atoi(this->seeData[BALL][DISTANCE].c_str())) < KICK_MARGIN)
                this->kickable = true;
            else
            {
                //mark that ball is far
                this->kickable = false;
            //turn to ball if direction is larger than view angle
            if (abs(atoi(this->seeData[BALL][DIRECTION].c_str())) > VIEW_ANGLE)
                *this->player > this->seeData[BALL][DIRECTION].c_str();
            else
                //if we are looking at the ball run towards ball
                this->run(atof(this->seeData[BALL][DISTANCE].c_str()));


            }

        }
        //else - find the ball
        else
            this->findBall();
    }
}
/*************************************************************************
* function name:stealBall*
* The Input:pair<float,int>*
* The output:           *
* The Function operation:steals the ball from a destination*
*************************************************************************/
void Brain::stealBall(pair<float, string> ballLocation)
{
    if (!this->kickable)
    {
        //if ball is close enough mark it
        if (abs(ballLocation.first) < KICK_MARGIN)
            this->kickable = true;
        else
        {
            //mark that ball is far
            this->kickable = false;

            //turn to ball if direction is larger than view angle
            if (abs(atoi(ballLocation.second.c_str())) > VIEW_ANGLE)
                *this->player > ballLocation.second.c_str();
            else
            //run towards ball
                this->run(ballLocation.first);
        }
    }




}
/*************************************************************************
* function name:goTowards*
* The Input:string*
* The output:           *
* The Function operation:goes towards give destination*
*************************************************************************/
void Brain::goTowards(const string destination)
{
    //if the destination is visible
    if (this->seeData.count(destination) == 1)
    {
        //turn towards destination if direction is larger than VIEW_ANGLE
        if (abs(atoi(this->seeData[destination][DIRECTION].c_str())) > VIEW_ANGLE)
            *player > this->seeData[destination][DIRECTION].c_str();
        else
            //run towards destination
            this->run(atof(this->seeData[destination][DISTANCE].c_str()));
    }
    //search for destination
    else
        *player > SEARCH;
}
/*************************************************************************
* function name:pass*
* The Input:int, string*
* The output:               *
* The Function operation:passes the ball to a target using the distance and direction*
*************************************************************************/
void Brain::pass(float distance, string direction)
{
    //get effective power
    double effectivePower = distance / (ballDistParam);

    //consider average noise
    effectivePower /= AVG_NOISE;

    //get how hard you should kick
    int kickStrength = (effectivePower / KICK_POWER_RATE);

    //convert to string
    stringstream convert;

    convert << kickStrength;

    //kick
    *this->player <= convert.str().c_str() >= direction.c_str();

    //indicate that ball is no longer kickable
    this->kickable = false;
}

/*************************************************************************
* function name:passTo*
* The Input:string*
* The output:               *
* The Function operation:passes the ball to a team mate*
*************************************************************************/
void Brain::passTo(const string teamMate)
{
    //if ball is nearby
    if (this->kickable)
    {
        //build teamMate name
        string playerName = TEAM_MATE;
        playerName += SPACE;
        playerName += teamMate;
        //if specific player isn't visible
        if (this->seeData.count(playerName) == 0)
            playerName = TEAM_MATE;
        //if the player could be found
        if (this->seeData.count(playerName) == 1)
        {
            //if direction is larger than VIEW_ANGLE - focus on him
            if (abs(atoi(this->seeData[playerName][DIRECTION].c_str())) > VIEW_ANGLE)
                *player > this->seeData[playerName][DIRECTION].c_str();
            else
            {
                //notify player
                this->player->say(teamMate.c_str());

                //kick towards him
                this->pass(atof(this->seeData[playerName][DISTANCE].c_str()),this->seeData[playerName][DIRECTION].c_str());

            }
        }
        //search for specific teamMate
        else
            *this->player > SEARCH;
    }
    else
        this->stealBall();
}
/*************************************************************************
* function name:passTo*
* The Input:pari<unsigned int, int>*
* The output:               *
* The Function operation:passes the ball team mate at given location*
*************************************************************************/
void Brain::passTo(const pair<unsigned int, string> teamMate, string name)
{
    //if ball is nearby
    if (this->kickable)
    {
        //if direction is larger than VIEW_ANGLE - focus on him
        if (abs(atoi(teamMate.second.c_str())) > VIEW_ANGLE)
            *this->player > teamMate.second.c_str();
        else
        {
            //notify player
            this->player->say(name.c_str());

            //kick towards him
            this->pass(teamMate.first, teamMate.second);

        }
    }
    else
        this->stealBall();
}

/*************************************************************************
* function name:findBall*
* The Input:            *
* The output:           *
* The Function operation:looks for the ball*
*************************************************************************/
void Brain::findBall()
{
    //if the ball wasn't found
    if (this->seeData.count(BALL) == 0)
    {
        //turn around
        *player > SEARCH;
    }
}

/*************************************************************************
* function name:tryScore*
* The Input:            *
* The output:           *
* The Function operation:tries to score*
*************************************************************************/
void Brain::tryScore()
{

    //if ball is nearby - search for goal
    if (this->kickable)
    {
        //if the goal is visible
        if (this->seeData.count(theirGoal) == 1)
        {
            string direction  = this->seeData[this->theirGoal][DIRECTION].c_str();
            //kick towards it
            this->pass(atof(this->seeData[this->theirGoal][DISTANCE].c_str()), direction);

            //focus on goal if not close enough
            if (abs(atoi(direction.c_str())) > VIEW_ANGLE)
                *this->player > direction.c_str();
        }
        else
            //search
            *player > SEARCH;
    }
    else
        //go to ball
        this->stealBall();
}
/*************************************************************************
* function name:tryScore*
* The Input:pair <unsigned int, int>*
* The output:           *
* The Function operation:tries to score using a goal location*
*************************************************************************/
void Brain::tryScore(pair <unsigned int, string> goalLocation)
{
    //if ball is nearby - search for goal
    if (this->kickable)
    {
        //kick towards it
        this->pass(goalLocation.first, goalLocation.second);

        //focus on goal if not close enough
        if (abs(atoi(goalLocation.second.c_str())) > VIEW_ANGLE)
            *this->player > goalLocation.second.c_str();


    }
    else
        //go to ball
        this->stealBall();
}

/*************************************************************************
* function name:tryScore*
* The Input:pair <float, int>*
* The output:           *
* The Function operation:tries to score using the ball's location*
*************************************************************************/
void Brain::tryScore(pair<float, string> ballLocation)
{
    this->stealBall(ballLocation);
}
/*************************************************************************
* function name:handleHearMsg*
* The Input:            *
* The output:           *
* The Function operation:handles hear messages*
*************************************************************************/
void Brain::handleHearMsg()
{
    //if message from referee
    if (hearData.size() == REFEREE_MSG_SIZE)
    {
        //get msg
        string refereeMsg = hearData[REFEREE_MSG_SIZE -1];

        //if we can't keep playing - stop playing
        if (refereeMsg == TIME_OVER || refereeMsg == BEFORE_KICK_OFF ||refereeMsg == freeKickThem || refereeMsg == cornerKickThem || refereeMsg == kickOffThem
                || refereeMsg == goalKickThem || refereeMsg == offsideUs || refereeMsg == goalUs || refereeMsg == goalThem)
        {
            //signal hear data is old
            this->newHearData = false;

            //unlock it
            this->lockHearData = false;

            bool stopPlay = true;

            //wait to be released
            while (stopPlay)
            {
                //if we got a new msg
                if(this->newHearData)
                {
                    //and it's from the referee
                    if (hearData.size() == REFEREE_MSG_SIZE)
                    {
                        //get his msg
                        refereeMsg = hearData[REFEREE_MSG_SIZE -1];

                        //check if we can get back to playing
                        if (refereeMsg == PLAY_ON || refereeMsg == DROP_BALL || refereeMsg == kickOffUs ||
                                refereeMsg == freeKickUs || refereeMsg == cornerKickUs || refereeMsg == goalKickUs || refereeMsg == offsideThem)
                            stopPlay = false;
                    }

                    //if msg didn't release us
                    if(stopPlay)
                    {
                        //indicate data is old
                        this->newHearData = false;

                        //unlock it
                        this->lockHearData = false;
                    }
                }
            }


        }

    }
    //if message is from another player and he is calling this player
    else if (hearData.size() >= PLAYER_MSG_SIZE && hearData[HEAR_PLAYER_MSG] == this->playerNum)
    {
        //turn to player
        *this->player > hearData[HEAR_PLAYER_ANGLE].c_str();
    }

    //signal hear data is old
    this->newHearData = false;

    //unlock it
    this->lockHearData = false;
}
#define TIME_OVER "time_over"
#define KICK_OFF "kick_off_"
#define KICK_IN "kick_in_"
#define FREE_KICK "free_kick_"
#define CORNER_KICK "corner_kick_"
#define GOAL_KICK "goal_kick_"
#define GOAL_SCORE "goal_"
#define OFFSIDE "offside_"
