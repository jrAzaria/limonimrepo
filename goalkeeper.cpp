#include "goalkeeper.h"
#define CATCH "catch"

/*************************************************************************
* function name:constructor*
* The Input:int int int int string string*
* The output:           *
* The Function operation:makes a brain for a goalie and make bounds, sets x y points for move
*  sets defense and offense points*
*************************************************************************/
GoalKeeper::GoalKeeper(pair<double, double> infX, pair<double, double> supX, pair<double, double> infY, pair<double, double> supY, string moveX, string moveY, pair<string, string> defense, pair<string,string> offense): Brain(infX, supX, infY, supY, moveX, moveY, defense, offense)
{
    catched = false;

    //setting parameters for each function,
    vector<int> functionCost(NUM_OF_PARAM);
    functionCost[tmMtNrBll] = HIGH_MOTIVATION;
    functionCost[rvlNearBll] = NO_MOTIVATION;
    functionCost[seeGl] = NO_MOTIVATION;
    functionCost[tmMtNr] = NO_MOTIVATION;
    functionCost[bllKck] = NO_MOTIVATION;
    functionCost[bllNr] = NO_MOTIVATION;
    functionCost[otOfBnds] = HIGH_MOTIVATION;
    this->functions[TO_POST] = functionCost;

    functionCost[tmMtNrBll] = NO_MOTIVATION;
    functionCost[rvlNearBll] = HIGH_MOTIVATION;
    functionCost[seeGl] = L_MED_MOTIVATION;
    functionCost[tmMtNr] = LOW_MOTIVATION;
    functionCost[bllKck] = NO_MOTIVATION;
    functionCost[bllNr] = HIGH_MOTIVATION;
    functionCost[otOfBnds] = -HIGH_MOTIVATION;
    this->functions[STEAL_BALL] = functionCost;

    functionCost[tmMtNrBll] = NO_MOTIVATION;
    functionCost[rvlNearBll] = MED_MOTIVATION;
    functionCost[seeGl] = NO_MOTIVATION;
    functionCost[tmMtNr] = HIGH_MOTIVATION;
    functionCost[bllKck] = HIGH_MOTIVATION;
    functionCost[bllNr] = LOW_MOTIVATION;
    functionCost[otOfBnds] = -HIGH_MOTIVATION;
    this->functions[PASS_TO] = functionCost;

    functionCost[tmMtNrBll] = NO_MOTIVATION;
    functionCost[rvlNearBll] = MED_MOTIVATION;
    functionCost[seeGl] = HIGH_MOTIVATION;
    functionCost[tmMtNr] = NO_MOTIVATION;
    functionCost[bllKck] = HIGH_MOTIVATION;
    functionCost[bllNr] = LOW_MOTIVATION;
    functionCost[otOfBnds] = -HIGH_MOTIVATION;
    this->functions[TRY_SCORE] = functionCost;

    functionCost[tmMtNrBll] = NO_MOTIVATION;
    functionCost[rvlNearBll] = HIGH_MOTIVATION;
    functionCost[seeGl] = NO_MOTIVATION;
    functionCost[tmMtNr] = LOW_MOTIVATION;
    functionCost[bllKck] = HIGH_MOTIVATION;
    functionCost[bllNr] = LOW_MOTIVATION;
    functionCost[otOfBnds] = -HIGH_MOTIVATION;
    this->functions[CATCH] = functionCost;
}
/*************************************************************************
* function name:decideAction*
* The Input:            *
* The output:           *
* The Function operation:decides what's the best next move*
*************************************************************************/
void GoalKeeper::decideAction()
{
    //wait for data to be locked
    while (!this->lockSeeData || !this->lockSenseData)
    {
        //if a hear message was received
        if (newHearData)
        {
            //take care of it
            this->handleHearMsg();
        }

    }

    //get function to do
    string function = options->getOptions(this->seeData, this->senseData, this->bounds, this->kickable);

    //use the right function
    if(TO_POST == function)
        this->toPost();
    else if(STEAL_BALL == function)
    {
        if (options->checkBallFound())
            this->stealBall(options->getBallLocation());
        else
            this->stealBall();
    }
    else if(PASS_TO == function)
    {
        //if we found a team mate
        if (this->options->checkTeamMateFound())
            //if we found his number - pass to his location and number
            if (this->options->checkTeamMateNumberFound())
                this->passTo(this->options->getTeamMateLocation(), this->options->getTeamMateNumber());
            else
                //pass to his location
                this->passTo(this->options->getTeamMateLocation());
        else
            this->passTo(NO_ONE);
    }
    else if(TRY_SCORE == function)
    {
        //if we can kick the ball and we know where the goal is
        if (this->kickable && this->options->checkGoalfound())
        {
            this->tryScore(this->options->getGoalLocation());
        }
        //if we can't kick the ball but know where it is
        else
            if (!this->kickable && this->options->checkBallFound())
            {
                this->tryScore(this->options->getBallLocation());
            }
        //if we can't help the function
            else
                this->tryScore();
    }
    else if (CATCH == function && !catched)
    {
        //if we found the ball
        if (this->options->checkBallFound())
        {
            //catch ball
            this->catchBall(this->options->getBallLocation().second);

            //indicate it was caught
            catched = true;
        }
        else
            this->stealBall();
    }
    else
    {
        //if we found a team mate
        if (this->options->checkTeamMateFound())
        {
            //if we found his number - pass to his location and number
            if (this->options->checkTeamMateNumberFound())
                this->passTo(this->options->getTeamMateLocation(), this->options->getTeamMateNumber());
            else
                //pass to his location
                this->passTo(this->options->getTeamMateLocation());
        }
        //if we can kick the ball and we know where the goal is
        else if (this->kickable && this->options->checkGoalfound())
        {
            this->tryScore(this->options->getGoalLocation());
        }
        //if we can't kick the ball but know where it is
        else
            if (!this->kickable && this->options->checkBallFound())
            {
                this->tryScore(this->options->getBallLocation());
            }
        //if we can't help the function
            else
                this->tryScore();

    }

    //unlock data
    this->lockSeeData = false;
    this->lockSenseData = false;
}
/*************************************************************************
* function name:catchBall*
* The Input:string*
* The output:           *
* The Function operation:catches a ball*
*************************************************************************/
void GoalKeeper::catchBall(string direction)
{
    this->player->catchBall(direction.c_str());

}
