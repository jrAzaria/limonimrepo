#ifndef COACH_H
#define COACH_H
#include "udp.h"
#include "dataanalyzer.h"
#include "vector"
#include <algorithm>

class Coach
{
//private:
public:
    UDP * client;
    bool offenseStrategy;
    vector<DataAnalyzer *> observersList;
    static bool coachMade;
    bool dataInUse;
    string seeGlobalData;
    unsigned int count;
    unsigned int time;

    /*************************************************************************
    * function name:play*
    * The Input:                    *
    * The output:               *
    * The Function operation:opens 3 threads and manages the game*
    *************************************************************************/
    void play();
    /*************************************************************************
    * function name:parseTime*
    * The Input:string*
    * The output:unsigned int*
    * The Function operation:finds the time the message was sent*
    *************************************************************************/
    unsigned int parseTime(const string text);
    /*************************************************************************
    * function name:launchReceive*
    * The Input:this*
    * The output:void * *
    * The Function operation:static function that launches a reaceiving thread*
    *************************************************************************/
    static void* launchReceive(void* thisObject);
    /*************************************************************************
    * function name:launchDecideStrategy*
    * The Input:this*
    * The output:void * *
    * The Function operation:static function that launches a decide-strategy*
    *************************************************************************/
    static void* launchDecideStrategy(void* thisObject);

    /*************************************************************************
    * function name:get*
    * The Input:              *
    * The output:void * *
    * The Function operation:handles input from server*
    *************************************************************************/
    void*  get();

    /*************************************************************************
    * function name:send*
    * The Input:              *
    * The output:void * *
    * The Function operation:sends the parsed data to brain*
    *************************************************************************/
    void * send();
    /*************************************************************************
    * function name:decideStrategy*
    * The Input:const string data*
    * The output:           *
    * The Function operation:decides on strategy base on data from server*
    *************************************************************************/
    void * decideStrategy();
    /*************************************************************************
    * function name:receiveInput*
    * The Input:        *
    * The output:           *
    * The Function operation:receives input from server and decides on strategy*
    *************************************************************************/
    void receiveInput();

    /*************************************************************************
    * function name:notify*
    * The Input:        *
    * The output:bool   *
    * The Function operation:notifies all observers when strategy changes*
    *************************************************************************/
    void notify(bool offense);
    /*************************************************************************
    * function name:Coach*
    * The Input:            *
    * The output:           *
    * The Function operation:deafult constructor*
    *************************************************************************/
    Coach();
//public:
    /*************************************************************************
    * function name:getCoach*
    * The Input:            *
    * The output:           *
    * The Function operation:makes a coach if there isn't already one*
    *************************************************************************/
    Coach * getCoach();
    /*************************************************************************
    * function name:addObserver*
    * The Input:* DataAnalyzer*
    * The output:  bool         *
    * The Function operation:signs up the observer and adds him to the observers list*
    *************************************************************************/
    bool addObserver(DataAnalyzer *observer);



    /*************************************************************************
    * function name:parseTitle*
    * The Input:string*
    * The output:string*
    * The Function operation:returns the title of the message from server*
    *************************************************************************/
    string parseTitle(const string text);

    /*************************************************************************
    * function name:parseTeamLocations*
    * The Input:string*
    * The output: vector< pair<double, double> >*
    * The Function operation:returns a vector with all the team's players locations*
    *************************************************************************/
    vector< pair<double, double> > parseTeamLocations (const string text);

    /*************************************************************************
    * function name:parseBallLocation*
    * The Input:string*
    * The output: pair<double, double>*
    * The Function operation:returns a pair with the ball's location*
    *************************************************************************/
    pair<double, double> parseBallLocation (const string text);

    /*************************************************************************
    * function name:isBallInOurTeam*
    * The Input:const string*
    * The output: bool*
    * The Function operation: receives a see_global message and returns if the ball
    * is held by our team or not
    *************************************************************************/
    bool isBallInTeam (const string text);


    /*************************************************************************
    * function name:calcDistance*
    * The Input:pair<double, double>, pair<double, double>*
    * The output: double*
    * The Function operation:receives 2 objects locations and returns the distance
     between the objects*
    *************************************************************************/
    double calcDistance (pair<double, double> player, pair<double, double> ball);

    ~Coach();

    /*************************************************************************
     * function name:convertStringToDouble()*
     * The Input: pair<string, string>*
     * The output: pair<double, double>*
     * The Function operation: converts a pair of string to a pair of doubles
     * and returns
     *************************************************************************/
    pair<double, double> convertStringToDouble (pair<string, string> toConvert);
};

#endif // COACH_H
