#ifndef DATAANALYZER_H
#define DATAANALYZER_H
#include "udp.h"
#include "brain.h"
#include <map>
#include <vector>
#include <string>
#include <iostream>



//#include "brain.h"
class DataAnalyzer
{
private:
    UDP * client;
    Brain * brain;
    char side;
    char otherSide;
    bool onField;
    string seeData;
    string senseData;
    string hearData;
    bool seeDataInUse;
    bool senseDataInUse;
    bool hearDataInUse;
    bool strategy;
    unsigned int time;
    unsigned int myScore;
    unsigned int theirScore;
private:

    /*************************************************************************
    * function name:get*
    * The Input:              *
    * The output:void * *
    * The Function operation:handles input from server*
    *************************************************************************/
    void*  get();
    /*************************************************************************
    * function name:send*
    * The Input:              *
    * The output:void * *
    * The Function operation:sends the parsed data to brain*
    *************************************************************************/
    void * send();
    /*************************************************************************
    * function name:launchReceive*
    * The Input:this*
    * The output:void * *
    * The Function operation:static function that launches a reaceiving thread*
    *************************************************************************/
    static void* launchReceive(void* thisObject);
    /*************************************************************************
    * function name:launchDeliver*
    * The Input:this*
    * The output:void * *
    * The Function operation:static function that launches a sending thread*
    *************************************************************************/
    static void* launchDeliver(void* thisObject);
    /*************************************************************************
    * function name:launchHear
    * The Input:this*
    * The output:void * *
    * The Function operation:static function that helps an object launch a sending hear thread*
    *************************************************************************/
    static void* launchHear(void* thisObject);
    /*************************************************************************
    * function name:findSide*
    * The Input:string*
    * The output:               *
    * The Function operation:finds which side are you on*
    *************************************************************************/
    void findSide(const string text);
    /*************************************************************************
    * function name:parseTitle*
    * The Input:string*
    * The output:string*
    * The Function operation:returns the title of the message from server*
    *************************************************************************/
    string parseTitle(const string text);
    /*************************************************************************
    * function name:parseTime*
    * The Input:string*
    * The output:unsigned int*
    * The Function operation:finds the time the message was sent*
    *************************************************************************/
    unsigned int parseTime(const string text);
    /*************************************************************************
    * function name:parseMyScore*
    * The Input:string*
    * The output:unsigned int*
    * The Function operation:finds my score from a score data message*
    *************************************************************************/
    unsigned int parseMyScore(const string text);
    /*************************************************************************
    * function name:parseTheirScore*
    * The Input:string*
    * The output:unsigned int*
    * The Function operation:finds their score from a score data message*
    *************************************************************************/
    unsigned int parseTheirScore(const string text);
    /*************************************************************************
    * function name:parseDistance*
    * The Input:string*
    * The output:string*
    * The Function operation:finds the distance*
    *************************************************************************/
    string parseDistance(const string text);
    /*************************************************************************
    * function name:parseDirection*
    * The Input:string*
    * The output:string*
    * The Function operation:finds the direction*
    *************************************************************************/
    string parseDirection(const string text);
    /*************************************************************************
    * function name:parseOpeningReferee*
    * The Input:string*
    * The output:string*
    * The Function operation:finds the referee's opening msg*
    *************************************************************************/
    string parseOpeningReferee(const string text);
    /*************************************************************************
    * function name:parsePlayerNum*
    * The Input:string*
    * The output:string*
    * The Function operation:finds the player's number*
    *************************************************************************/
    string parsePlayerNum(const string text);


public:
    /*************************************************************************
    * function name:DataAnalyzer*
    * The Input:              *
    * The output:object*
    * The Function operation:creates a data analyzer object and inits all vars*
    *************************************************************************/
    DataAnalyzer(Brain * newBrain);
    /*************************************************************************
    * function name:~DataAnalyzer*
    * The Input:              *
    * The output:object*
    * The Function operation:closes communication and deletes all objects*
    *************************************************************************/
    ~DataAnalyzer();
    /*************************************************************************
    * function name:play*
    * The Input:                    *
    * The output:               *
    * The Function operation:opens 2 threads and plays the game*
    *************************************************************************/
    void play(bool goalie = false);

    /*************************************************************************
        * function name:parseSee*
        * The Input:string*
        * The output:map<string, vector <string>>*
        * The Function operation:returns a map in which each key
        * is a string holding the object's name and each value
        * is a vector of all object's parameters*
        *************************************************************************/
    map<string, vector<string> > parseSee(const string text);

    /*************************************************************************
        * function name:parsePlayerType*
        * The Input:string*
        * The output:map<string, vector<string> >*
        * The Function operation:returns a vector of all the player's parameters*
        *************************************************************************/
    map<string, vector<string> > parsePlayerType(const string text);


    /*************************************************************************
        * function name:parseSense*
        * The Input:string*
        * The output:map<string, vector <string>>*
        * The Function operation:returns a vector of vector of all sense parameters*
        *************************************************************************/
    map<string, vector <string> > parseSense(const string text);

    /*************************************************************************
        * function name:between*
        * The Input:under, target, above*
        * The output:bool*
        * The Function operation:template function that returns if target is between
        * under and above*
        *************************************************************************/
    template <typename T>
    inline bool between(T const& underTarget, T const& target, T const& aboveTarget)
    {
        return (target <= aboveTarget && target >= underTarget);
    }

    /*************************************************************************
        * function name:changeStrategy*
        * The Input:bool*
        * The output:           *
        * The Function operation:changes the strategy*
        *************************************************************************/
    void changeStrategy(bool offense);

    /*************************************************************************
         * function name:parseHear*
         * The Input:string*
         * The output:vector<string>*
         * The Function operation:returns a vector of all hear parameters*
         *************************************************************************/
    vector<string> parseHear(const string text);

    /*************************************************************************
         * function name:sendHear*
         * The Input:vector<string>*
         * The output:               *
         * The Function operation:sends hear parameters to brain*
         *************************************************************************/
    void *sendHear();

    /*************************************************************************
         * function name:endGame*
         * The Input:              *
         * The output:               *
         * The Function operation:closes the communication*
         *************************************************************************/
    void endGame();



};

#endif // DATAANALYZER_H














