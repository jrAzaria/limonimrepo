#include "striker.h"
#include <unistd.h>

/*************************************************************************
* function name:constructor*
* The Input:int int int int string string*
* The output:           *
* The Function operation:makes a brain for a striker and make bounds, gets x y points for move
* sets defense and offense points*
*************************************************************************/
Striker::Striker(pair<double, double> infX, pair<double, double> supX, pair<double, double> infY, pair<double, double> supY, string moveX, string moveY, pair<string, string> defense, pair<string,string> offense): Brain(infX, supX, infY, supY, moveX, moveY, defense, offense)
{
    //setting parameters for each function,
    vector<int> functionCost(NUM_OF_PARAM);
    functionCost[tmMtNrBll] = MED_MOTIVATION;
    functionCost[rvlNearBll] = NO_MOTIVATION;
    functionCost[seeGl] = NO_MOTIVATION;
    functionCost[tmMtNr] = NO_MOTIVATION;
    functionCost[bllKck] = NO_MOTIVATION;
    functionCost[bllNr] = NO_MOTIVATION;
    functionCost[otOfBnds] = HIGH_MOTIVATION;
    this->functions[TO_POST] = functionCost;

    functionCost[tmMtNrBll] = NO_MOTIVATION;
    functionCost[rvlNearBll] = MED_MOTIVATION;
    functionCost[seeGl] = L_MED_MOTIVATION;
    functionCost[tmMtNr] = LOW_MOTIVATION;
    functionCost[bllKck] = NO_MOTIVATION;
    functionCost[bllNr] = HIGH_MOTIVATION;
    functionCost[otOfBnds] = -LOW_MOTIVATION;
    this->functions[STEAL_BALL] = functionCost;

    functionCost[tmMtNrBll] = NO_MOTIVATION;
    functionCost[rvlNearBll] = MED_MOTIVATION;
    functionCost[seeGl] = NO_MOTIVATION;
    functionCost[tmMtNr] = HIGH_MOTIVATION;
    functionCost[bllKck] = MED_MOTIVATION;
    functionCost[bllNr] = LOW_MOTIVATION;
    functionCost[otOfBnds] = -LOW_MOTIVATION;
    this->functions[PASS_TO] = functionCost;

    functionCost[tmMtNrBll] = NO_MOTIVATION;
    functionCost[rvlNearBll] = L_MED_MOTIVATION;
    functionCost[seeGl] = HIGH_MOTIVATION;
    functionCost[tmMtNr] = NO_MOTIVATION;
    functionCost[bllKck] = M_HIGH_MOTIVATION;
    functionCost[bllNr] = LOW_MOTIVATION;
    functionCost[otOfBnds] = -LOW_MOTIVATION;
    this->functions[TRY_SCORE] = functionCost;
}


