#include "brainfactory.h"
#include <unistd.h>
//#include <stdlib.h>
#include <utility>
#define MAX_X_POS 52.5
#define MAX_X_NEG -52.5
#define MID_X_POS  35
#define MINUS_TWENTY -20
#define MIN_THIRTY_FIVE -35
#define PLUS_TWENTY 20
#define G_L "g l"
#define G_R "g r"
#define MIN_FORTY_SEVEN_STR "-47"
#define ZERO_STR "0"
#define MIN_FIFTEEN -15
#define PLUS_FIFTEEN 15
#define MIN_FORTY -40
#define PLUS_FORTY 40
#define MIN_TEN -10
#define PLUS_TEN 10
#define F_P_L_T "f p l t"
#define F_P_R_B "f p r b"
#define F_G_L_T "f g l t"
#define F_G_R_B "f g r b"
#define ZERO 0
#define MIN_THIRTY_SEVEN_STRING "-37"
#define F_P_L_C "f p l c"
#define F_P_R_C "f p r c"
#define F_C_T "f c t"
#define F_C_B "f c b"
#define MIN_TWENTY_FIVE_STR "-25"
#define F_C "f c"
#define F_P_R_T "f p r t"
#define PLUS_FIFTEEN_STR "15"
#define MINUS_FIFTEEN_STR "-15"
#define PLUS_TWENTY_FIVE_STR "25"
#define MINUS_SEVEN_STR "-7"
#define MINUS_TEN_STR "-10"
#define PLUS_SEVEN_STR "7"
#define PLUS_TWENTY_STR "20"
#define MINUS_TWENTY_STR "-20"
#define F_P_L_B "f p l b"

using namespace std;

BrainFactory::BrainFactory() {}

BrainFactory::~BrainFactory(){}

Brain * BrainFactory::createBrain(int brainType, int brainLocation){
 pair<double, double> minX;
 pair<double, double> minY;
 pair<double, double> maxX;
 pair<double, double> maxY;
 pair<string, string> defense;
 pair<string, string> offense;
 string move_x;
 string move_y;

 //GOALIE
   if(brainType == GOALKEEPER) {
      minX.first = MAX_X_NEG;
      minX.second =MID_X_POS;
      minY.first = MINUS_TWENTY;
      minY.second = MINUS_TWENTY;
      maxX.first = MIN_THIRTY_FIVE;
      maxX.second =MAX_X_POS;
      maxY.first = PLUS_TWENTY;
      maxY.second = PLUS_TWENTY;
      defense.first = G_L;
      defense.second = G_R;
      offense.first = G_L;
      offense.second = G_R;
      move_x = MIN_FORTY_SEVEN_STR;
      move_y = ZERO_STR;

      return new GoalKeeper (minX, maxX, minY, maxY, move_x, move_y, defense, offense);
 //DEFENDERS
   } else if (brainType == DEFENDER) {
       if(brainLocation == FIRST) {
           maxX.first = MIN_FIFTEEN;
           maxX.second =MAX_X_POS;
           minX.first = MAX_X_NEG;
           minX.second =PLUS_FIFTEEN;

           maxY.first =PLUS_FORTY;
           maxY.second =ZERO;
          minY.first =ZERO;
          minY.second = MIN_FORTY;
           defense.first = F_G_L_T;
           defense.second = F_G_R_B;
           offense.first = F_P_L_T;
           offense.second = F_P_R_B;
           move_x = MIN_THIRTY_SEVEN_STRING;
           move_y = MINUS_TWENTY_STR;
           return new Defender (minX, maxX, minY, maxY, move_x, move_y, defense, offense);
       } else if(brainLocation == SECOND) {
           maxX.first = MIN_FIFTEEN;
           maxX.second =MAX_X_POS;
           minX.first = MAX_X_NEG;
           minX.second =PLUS_FIFTEEN;

            maxY.first = PLUS_TWENTY;
            maxY.second = PLUS_TWENTY;
           minY.first = MINUS_TWENTY;
           minY.second = MINUS_TWENTY;
           defense.first = F_P_L_C;
           defense.second = F_P_R_C;
           offense.first = F_P_L_C;
           offense.second = F_P_R_C;
           move_x = MIN_THIRTY_SEVEN_STRING;
           move_y = ZERO_STR;
    return new Defender (minX, maxX, minY, maxY, move_x, move_y, defense, offense);
       } else if (brainLocation == THIRD) {
           maxX.first = MIN_FIFTEEN;
           maxX.second =MAX_X_POS;
           minX.first = MAX_X_NEG;
           minX.second =PLUS_FIFTEEN;
           maxY.first =ZERO;
           maxY.second =PLUS_FORTY;
          minY.first = MIN_FORTY;
          minY.second =ZERO;
           defense.first = "f g l b";
           defense.second = "f g r t";
           offense.first = F_P_L_B;
           offense.second = F_P_R_T;
           move_x = MIN_THIRTY_SEVEN_STRING;
           move_y = PLUS_TWENTY_STR;
           return new Defender (minX, maxX, minY, maxY, move_x, move_y, defense, offense);
    //MIDFIELDER
       } } else if (brainType == MIDFIELDER) {
       if (brainLocation == FIRST) {
       maxX.first = PLUS_TWENTY;
       maxX.second =PLUS_FORTY;
       minX.first = MIN_FORTY;
       minX.second = MINUS_TWENTY;
        maxY.first =PLUS_FORTY;
        maxY.second = MIN_TEN;
       minY.first =PLUS_TEN;
       minY.second = MIN_FORTY;
       defense.first = F_P_L_T;
       defense.second = F_P_R_B;
       offense.first = F_C_T;
       offense.second = F_C_B;
       move_x = MINUS_TWENTY_STR;
       move_y = MIN_TWENTY_FIVE_STR;
       return new Midfielder (minX, maxX, minY, maxY, move_x, move_y, defense, offense);
      } else if (brainLocation == SECOND) {
           maxX.first = PLUS_TWENTY;
           maxX.second =PLUS_FORTY;
           minX.first = MIN_FORTY;
           minX.second = MINUS_TWENTY;
           maxY.first = PLUS_TWENTY;
           maxY.second =PLUS_TEN;
           minY.first = MIN_TEN;
           minY.second = MINUS_TWENTY;
           defense.first = F_P_L_C;
           defense.second = F_P_R_C;
           offense.first = F_C;
           offense.second = F_C;
           move_x = MINUS_TWENTY_STR;
           move_y = MINUS_FIFTEEN_STR;
           return new Midfielder (minX, maxX, minY, maxY, move_x, move_y, defense, offense);
       } else if (brainLocation == THIRD) {
           maxX.first = PLUS_TWENTY;
           maxX.second =PLUS_FORTY;
           minX.first = MIN_FORTY;
           minX.second = MINUS_TWENTY;
            maxY.first =PLUS_TEN;
            maxY.second = PLUS_TWENTY;
           minY.first = MINUS_TWENTY;
           minY.second = MIN_TEN;
           defense.first = F_P_L_C;
           defense.second = F_P_R_C;
           offense.first = F_C;
           offense.second = F_C;
           move_x = MINUS_TWENTY_STR;
           move_y = PLUS_FIFTEEN_STR;
           return new Midfielder (minX, maxX, minY, maxY, move_x, move_y, defense, offense);
       } else if (brainLocation == FOURTH) {
           maxX.first = PLUS_TWENTY;
           maxX.second =PLUS_FORTY;
           minX.first = MIN_FORTY;
           minX.second = MINUS_TWENTY;
            maxY.first = MIN_TEN;
            maxY.second =PLUS_FORTY;
           minY.first = MIN_FORTY;
           minY.second =PLUS_TEN;
           defense.first = F_P_L_B;
           defense.second = F_P_R_T;
           offense.first = F_C_B;
           offense.second = F_C_T;
           move_x = MINUS_TWENTY_STR;
           move_y = PLUS_TWENTY_FIVE_STR;
           return new Midfielder (minX, maxX, minY, maxY, move_x, move_y, defense, offense);
    //STRIKER
       } } else if (brainType == STRIKER) {
   }  if(brainLocation == FIRST) {
       maxX.first =MAX_X_POS;
       maxX.second =PLUS_TEN;
       minX.first = MIN_TEN;
       minX.second = MAX_X_NEG;
       maxY.first =PLUS_FORTY;
       maxY.second =ZERO;
       minY.first =ZERO;
       minY.second = MIN_FORTY;
       defense.first = F_C_T;
       defense.second = F_C_B;
       offense.first = F_P_R_T;
       offense.second = F_P_L_B;
       move_x = MINUS_SEVEN_STR;
       move_y = MINUS_SEVEN_STR;
       return new Striker (minX, maxX, minY, maxY, move_x, move_y, defense, offense);
   } else if(brainLocation == SECOND) {
       maxX.first =MAX_X_POS;
       maxX.second =PLUS_TEN;
       minX.first = MIN_TEN;
       minX.second = MAX_X_NEG;
       maxY.first = PLUS_TWENTY;
       maxY.second = PLUS_TWENTY;
      minY.first = MINUS_TWENTY;
      minY.second = MINUS_TWENTY;
       defense.first = F_C;
       defense.second = F_C;
       offense.first = F_P_R_C;
       offense.second = F_P_L_C;
       move_x = MINUS_TEN_STR;
       move_y = ZERO_STR;
       return new Striker (minX, maxX, minY, maxY, move_x, move_y, defense, offense);
   } else if (brainLocation == THIRD) {
       maxX.first =MAX_X_POS;
       maxX.second =PLUS_TEN;
       minX.first = MIN_TEN;
       minX.second = MAX_X_NEG;
       maxY.first = ZERO;
       maxY.second = PLUS_FORTY;
       minY.first = MIN_FORTY;
       minY.second =ZERO;
       defense.first = F_C_B;
       defense.second = F_C_T;
       offense.first = F_P_R_B;
       offense.second = F_P_L_T;
       move_x = MINUS_SEVEN_STR;
       move_y = PLUS_SEVEN_STR;
       return new Striker (minX, maxX, minY, maxY, move_x, move_y, defense, offense);
   }

       return NULL;
}


