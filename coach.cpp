/***************************************************************************
 * Students Name:Yehonatan Azaria, Meirav Ben Itzhak'    *
 * Exercise Name: ex5-6   *
 * File description:Coach implementation    *
 ****************************************************************************/
#include "coach.h"
#include "udp.h"
#include <iostream>
#include "string.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <map>
#include <stdlib.h>
#include <utility>
#include <string>
#include <pthread.h>
#include <vector>
#include <sstream>
#include <math.h>

#define SPACE " "
#define ZERO 0
#define ONE 1
#define LIMONIM_STR "limonim"
#define OTHER_TEAM_STR "otherTeam"
#define MERCHAOT "\""
#define BALL_STR "(b)"
#define COACH_INIT_MSG "init limonim(version 15)"
#define START "init"
#define SENSE "sense_body"
#define SEE "see"
#define HEAR "hear"
#define SCORE "score"
#define TYPE "player_type"
#define BALL "(b"
#define GOAL "(g"
#define SEARCH "45"
#define SEARCH_NUM 45
#define CIRCLE "360"
#define FULL_SWEEP 360
#define SOFT "33"
#define MILD "66"
#define STRONG "100"
#define MAX_RUN 1.00
#define MAX_KICK 30
#define MIN_KICK_DIST 0.6
#define LEFT 'l'
#define RIGHT 'r'
#define VIEW_ANGLE 15
#define END_PARENTHESIS ")"
#define INIT_COMMAND "(init limonim (version 15))"
#define PORT 6000
#define NUM_OF_THREADS 3
#define AFTER_FIRST_PARENTHESIS 1
#define HALF_TIME 3000
#define GAME_OVER 6000
#define BEFORE_KICK_OFF 0
#define SECONDS_AFTER_GOAL 4
#define SECONDS_AFTER_LOSE 2
#define SEE_GLOBAL "see_global"

//////need to add launce parse!!!!!!
/*************************************************************************
 * function name:Coach*
 * The Input:playersVector-vector of players  *
 * The output:*
 * The Function operation:create a coach object by creating a new UDP*
 * client
 *************************************************************************/
Coach::Coach(){

    //creat client
    client = new UDP(6002);

    //connect to server
    client->deliver(COACH_INIT_MSG, sizeof(COACH_INIT_MSG));

    //data isn't being used
    dataInUse = false;
}

/*************************************************************************
 * function name:~Coach()*
 * The Input: *
 * The output:*
 * The Function operation:delete the coach object and close the connection*
 *************************************************************************/
Coach::~Coach() {

    //close and delete client
    if (client) {
        client->finish();
        delete client;
    }
}

/*************************************************************************
 * function name:play()*
 * The Input: *
 * The output:*
 * The Function operation:creates 3 threads: 1 for receiving data from the serer,
 * 2 for analyzing it, 3 for sending notifying messages to the listeners
 *************************************************************************/
//////needs to be fixed
void Coach::play() {

    void * returnStatus;
    pthread_t newThreads[NUM_OF_THREADS];

    string openingMessage;
    //find the opening message
    do {
        openingMessage = client->mimicTcpReceive();

    } while (parseTitle(openingMessage)!= START);

    string command = "(eye on)";
    client->deliver(command.c_str(), command.size() + 1);

    //creates first thread for receiving messages
    int threadCheck = pthread_create(&newThreads[0], NULL,
            &Coach::launchReceive, this);
    //check that thread was created
    if (threadCheck) {
        cout << "thread number " << 1 << "failed with " << threadCheck << endl;
        exit(-1);
    }

    //create second thread for parsing the received messages
    threadCheck = pthread_create(&newThreads[1], NULL,
            &Coach::launchDecideStrategy, this);
    //check that thread was created
    if (threadCheck) {
        cout << "thread number " << 2 << "failed with " << threadCheck << endl;
        exit(-1);
    }
    //wait for threads to finish
    for (int i = 0; i < 2; ++i) {
        threadCheck = pthread_join(newThreads[i], &returnStatus);
        if (threadCheck) {
            cout << "thread number " << i << "failed with " << threadCheck
                    << endl;
            exit(-1);
        }
    }

    pthread_exit(NULL);
}
//////might need another thread for notifying of strategy change

/*************************************************************************
 * function name:launchReceive*
 * The Input:thisObject-object to run its function in the thread *
 * The output:*
 * The Function operation:run the get function of thisObject in the thread*
 *************************************************************************/
void * Coach::launchReceive(void * thisObject) {

    Coach * obj = (Coach*) thisObject;
    return obj->get();
}
/*************************************************************************
 * function name:launchDecideStrategy*
 * The Input:thisObject-object to run its function in the thread *
 * The output:*
 * The Function operation:run the send function of thisObject in the thread*
 *************************************************************************/
void * Coach::launchDecideStrategy(void * thisObject) {

    Coach * obj = (Coach*) thisObject;
    return obj->decideStrategy();
}

/*************************************************************************
 * function name:get()*
 * The Input: *
 * The output:*
 * The Function operation:get data from server as long as the game is running
 *************************************************************************/
void * Coach::get() {

    string text, title;
   do {
        //get data
         text = client->mimicTcpReceive();
         title = this->parseTitle(text);
         time = parseTime(text);
         if ((title == SEE_GLOBAL) && !dataInUse) {
             //copy data so it can be analyzed
             seeGlobalData = text.substr();

             //data is now being used, lock
             dataInUse = true;
}
    } while (time!=GAME_OVER);

    pthread_exit(NULL);

}

/*************************************************************************
 * function name:decideStrategy()*
 * The Input: *
 * The output:*
 * The Function operation:analyzes the data from the server and notifies the listeners in case
 * strategy needs to be changed
 *************************************************************************/
void * Coach::decideStrategy() {
    string dat;
    do {
    bool changeToDefense = false;
    if(this->dataInUse)
    dat = this->seeGlobalData.substr();
      changeToDefense = this->isBallInTeam(dat);
  if (changeToDefense) {
      this->count += 1;
  }
  if (count >= 3) {
      this->notify(changeToDefense);
      count = 0;
  }
    } while (time!=GAME_OVER);

    pthread_exit(NULL);
}
/*************************************************************************
* function name:parseTime*
* The Input:string*
* The output:unsigned int*
* The Function operation:finds the time the message was sent*
*************************************************************************/
unsigned int Coach::parseTime(const string text)
{
    unsigned int textTime = time;

    string title = parseTitle(text);
    //if this is message has time
    if (title == SEE || title == SENSE || title == SCORE)
    {
        //find the first space
        size_t firstSpace = text.find_first_of(SPACE);
        //find the time
        string tempTime = text.substr(firstSpace + 1, text.find_first_of(SPACE,firstSpace+1) -(firstSpace+1));
        textTime = atoi(tempTime.c_str());

    }

    return textTime;
}
/*************************************************************************
* function name:parseTitle*
* The Input:string*
* The output:string*
* The Function operation:returns the title of the message from server*
*************************************************************************/
string Coach::parseTitle(const string text)
{
    string returnString;

    //find the first word
    returnString = text.substr(AFTER_FIRST_PARENTHESIS, text.find_first_of(SPACE) - 1);
    return returnString;
}

/*************************************************************************
 * function name:send()*
 * The Input: *
 * The output:*
 * The Function operation:send data to server as long as the game is running
 *************************************************************************/
void * Coach::send() {

    while (true) { //change to while gme over

        cout<<"happy"<<endl;
    }

}

/*************************************************************************
 * function name:parseBallLocation()*
 * The Input: string*
 * The output: pair<double, double*
 * The Function operation: pulls the ball's location out of a given see_global
 * message
 *************************************************************************/
pair<double, double> Coach::parseBallLocation(const string text) {
    unsigned int pos = ZERO;
    string xPos, yPos;
    ///double x, y;
    unsigned int leftLim, rightLim, len;
    //finds the ball object in the see_global message
    leftLim = text.find(BALL);
    //pulls out its x coordinate
    pos = leftLim + ONE;
    leftLim = text.find(SPACE, pos);
    pos = leftLim + ONE;
    rightLim = text.find(SPACE, pos);
    len = rightLim - leftLim;
    xPos = text.substr(leftLim, len);
    //and its y coordinate
    leftLim = rightLim + ONE;
    pos = leftLim;
    rightLim = text.find(SPACE, pos);
    yPos = text.substr(leftLim, len);
   //converts the coordinates from string to double
   pair<string, string> convert = make_pair(xPos, yPos);
  pair <double, double> toReturn = this->convertStringToDouble(convert);
   return toReturn;
}

/*************************************************************************
 *
 * function name:parseTeamLocations()*
 * The Input: string*
 * The output: vector< pair<double, double> >*
 * The Function operation: pulls the ball's location out of a given see_global
 * message
 *************************************************************************/
vector< pair<double, double> > Coach::parseTeamLocations(const string text) {
    unsigned int pos = ZERO;
    string xPos, yPos;
   vector< pair<double, double> > toReturn;
    unsigned long leftLim, rightLim, len;;
    while (pos < text.length()) {
 // ((p "pirateParsers" 1 goalie) 50 -0 0 0 143 0)
        leftLim = text.find_first_of(MERCHAOT, pos);
        //pos = leftLim + 1;
        rightLim = text.find_first_of(MERCHAOT, leftLim + 1);
         if((leftLim != string::npos) && (rightLim != string::npos)) {
            // unsigned int lim = leftLim + 1;
             leftLim += 1;
        len = rightLim - leftLim;
        string team = text.substr(leftLim, len);
        if (team != LIMONIM_STR) {

            pos = leftLim;
    leftLim = text.find(END_PARENTHESIS, pos);
    //finds the x
    pos = leftLim + ONE;
    leftLim = text.find_first_not_of(SPACE, pos);
    pos = leftLim + ONE;
    rightLim = text.find(SPACE, pos);
    len = rightLim - leftLim;
    xPos = text.substr(leftLim, len);
   //finds the y
    leftLim = rightLim + ONE;
    pos = leftLim;
    rightLim = text.find(SPACE, pos);
     len = rightLim - leftLim;
    yPos = text.substr(leftLim, len);
   //converts to a pair of doubles:
    pair<string, string> convert = make_pair(xPos, yPos);
   pair <double, double> addToVector = this->convertStringToDouble(convert);
     toReturn.push_back(addToVector);
     pos = rightLim + ONE;
        } else {
            pos = rightLim + ONE;
        } }  else {
          break;
        } }
    return toReturn;
}

/*************************************************************************
 * function name:convertStringToDouble()*
 * The Input: pair<string, string>*
 * The output: pair<double, double>*
 * The Function operation: converts a pair of string to a pair of doubles
 * and returns
 *************************************************************************/
 pair<double, double> Coach::convertStringToDouble(pair<string, string> toConvert) {

   //pulls out the strings into a single variable
   string xCon = toConvert.first;
   string yCon = toConvert.second;
//converts them to double
   double x = atof(xCon.c_str());
   double y = atof(yCon.c_str());
//puts them in a pair of doubles
   pair<double, double> toReturn = make_pair(x, y);
//returns
   return toReturn;
 }

 /*************************************************************************
 * function name:calcDistance*
 * The Input:pair<double, double>, pair<double, double>*
 * The output: double*
 * The Function operation:receives 2 objects locations and returns the distance
  between the objects*
 *************************************************************************/
 double Coach::calcDistance(pair<double, double> player, pair<double, double> ball) {
//pulls out the x and y coordinates of the objects into a single variable each
     double x1 = player.first;
     double x2 = ball.first;

     double y1 = player.second;
     double y2 = ball.second;
//calculates the x difference and the y difference
     double xDif = x2 - x1;
     double yDif = y2 - y1;
//calculates the powers of the differences
     double xDifSqr = pow(xDif, 2);
     double yDifSqr = pow(yDif, 2);
//calculates the sum of the powers
     double distSum = xDifSqr + yDifSqr;
//calculates the sum's square root
     double dist = sqrt(distSum);
//returns the distance
     return dist;
 }

 /*************************************************************************
 * function name:isBallInOurTeam*
 * The Input:const string*
 * The output: bool*
 * The Function operation: receives a see_global message and returns if the ball
 * is held by our team or not
 *************************************************************************/
 bool Coach::isBallInTeam(const string text) {
   //pulls out the ball's location
     pair<double, double> ball = this->parseBallLocation(text);
   //pulls out the team players' locations
     vector< pair<double, double> > players = this->parseTeamLocations(text);
   //checks if any of the players is close enough to the ball
     for(vector< pair<double, double> >::const_iterator it = players.begin();
         it != players.end(); ++it)
     {
         pair<double, double> player = *it;
         double distance = this->calcDistance(player , ball);
         if (distance <= 0.6) {
//if one of the players is found to be 0.6m or less away from the ball-returns true
             return true;
         }
     }
 //otherwise- returns false
   return false;
 }

 /*************************************************************************
 * function name:addObserver*
 * The Input: DataAnalyzer * observer*
 * The output: bool*
 * The Function operation: receives a DataAnalyzer and adds it to the vector
 * of listeners
 *************************************************************************/
 bool Coach::addObserver( DataAnalyzer * observer) {
     vector<DataAnalyzer*>::iterator temp = find (observersList.begin(), observersList.end(), observer);
     //Return false if the observer is already in the vector
     if ( temp != observersList.end() ) {
         return false;
     }else {
     observersList.push_back(observer);
     return true;
     }
 }


 /*************************************************************************
 * function name:notify*
 * The Input: r*
 * The output: bool*
 * The Function operation: updates all listeners to change their strategy to offense
 *************************************************************************/
 void Coach::notify(bool offense) {
     for (unsigned int i=0; i< observersList.size(); ++i)
     {
     observersList[i]->changeStrategy(offense);
     }
}

