/****************************************
* Student Name:Yonatan Azaria*
* Exercise Name:Multi-Threading*
* File description:UDP server and client*
****************************************/
#ifndef UDP_H
#define UDP_H
#include "network.h"

#include <iostream>

#include <sys/socket.h>

#include <netinet/in.h>
#include "string.h"

#include <arpa/inet.h>

#include <unistd.h>
using namespace std;

class UDP: public Network
{
private:
    //socket for communication
    int sock;
    //port number
    int port;
    //an endpoint address to connect to a socket, used for first communication (client-send, server-bind)
    struct sockaddr_in primarySockAddress;
    //an endpoint address to connect to a socket, used for further communication
    struct sockaddr_in serverReceiveAddress;
    //bool to state if this object is a server or not (aka client)
    bool isServer;
public:
    /*************************************************************************
    * function name:UDP*
    * The Input:port number and bool if server*
    * The output:TCP object*
    * The Function operation:makes a UDP server/client*
    *************************************************************************/
    UDP(int portNum, bool server = false);
    /*************************************************************************
    * function name:~UDP*
    * The Input:              *
    * The output:              *
    * The Function operation:default destructor*
    *************************************************************************/
    ~UDP();
    /*************************************************************************
    * function name:deliver*
    * The Input:receives data in char[] format and size of data*
    * The output:              *
    * The Function operation:sends the data to the server/client*
    *************************************************************************/
    void deliver(const char sendData[], int dataLength);
    /*************************************************************************
    * function name:receive*
    * The Input:              *
    * The output:returns received data in string format*
    * The Function operation:returnes data from the server/client*
    *************************************************************************/
    string receive();
    /*************************************************************************
    * function name:receive*
    * The Input:              *
    * The output:returns received data in string format*
    * The Function operation:mimics TCP receive function by updating the port number*
    *************************************************************************/
    string mimicTcpReceive();
    /*************************************************************************
    * function name:finish*
    * The Input:              *
    * The output:              *
    * The Function operation:closes the socket*
    *************************************************************************/
    void finish();
};

#endif // UDP_H


