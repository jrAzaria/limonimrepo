#ifndef DEFENDER_H
#define DEFENDER_H
#include "brain.h"

class Defender : public Brain
{
public:
    /*************************************************************************
    * function name:constructor*
    * The Input:int int int int string string string string*
    * The output:           *
    * The Function operation:makes a brain for a defender and make bounds, x & y for move, sets defense and offense points*
    *************************************************************************/
    Defender(pair<double, double> infX, pair<double, double> supX, pair<double, double> infY, pair<double, double> supY, string moveX, string moveY, pair<string, string> defense, pair<string,string> offense);
};

#endif // DEFENDER_H
