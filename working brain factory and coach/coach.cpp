/***************************************************************************
 * Students Name:Yehonatan Azaria, Meirav Ben Itzhak'    *
 * Exercise Name: ex5-6   *
 * File description:Coach implementation    *
 ****************************************************************************/
#include "coach.h"
#include "udp.h"
#include <iostream>
#include "string.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <map>
#include <stdlib.h>
#include <utility>
#include <string>
#include <pthread.h>
#include <vector>
#include <sstream>

#define SPACE " "
#define ZERO 0
#define ONE 1
#define LIMONIM_STR "limonim"
#define BALL_STR "(b)"
#define COACH_INIT_MSG "init limonim(version 15)"
#define START "init"
#define SENSE "sense_body"
#define SEE "see"
#define HEAR "hear"
#define SCORE "score"
#define TYPE "player_type"
#define BALL "(b"
#define GOAL "(g"
#define SEARCH "45"
#define SEARCH_NUM 45
#define CIRCLE "360"
#define FULL_SWEEP 360
#define SOFT "33"
#define MILD "66"
#define STRONG "100"
#define MAX_RUN 1.00
#define MAX_KICK 30
#define MIN_KICK_DIST 0.6
#define LEFT 'l'
#define RIGHT 'r'
#define VIEW_ANGLE 15
#define END_PARENTHESIS ")"
#define INIT_COMMAND "(init limonim (version 15))"
#define PORT 6000
#define NUM_OF_THREADS 3
#define AFTER_FIRST_PARENTHESIS 1
#define HALF_TIME 3000
#define GAME_OVER 6000
#define BEFORE_KICK_OFF 0
#define SECONDS_AFTER_GOAL 4
#define SECONDS_AFTER_LOSE 2
#define SEE_GLOBAL "see_global"

//////need to add launce parse!!!!!!
/*************************************************************************
 * function name:Coach*
 * The Input:playersVector-vector of players  *
 * The output:*
 * The Function operation:create a coach object by creating a new UDP*
 * client
 *************************************************************************/
Coach::Coach(){

    //creat client
    client = new UDP(6002);

    //connect to server
    client->deliver(COACH_INIT_MSG, sizeof(COACH_INIT_MSG));

    //data isn't being used
    dataInUse = false;
}

/*************************************************************************
 * function name:~Coach()*
 * The Input: *
 * The output:*
 * The Function operation:delete the coach object and close the connection*
 *************************************************************************/
Coach::~Coach() {

    //close and delete client
    if (client) {
        client->finish();
        delete client;
    }
}

/*************************************************************************
 * function name:play()*
 * The Input: *
 * The output:*
 * The Function operation:creates 3 threads: 1 for receiving data from the serer,
 * 2 for analyzing it, 3 for sending notifying messages to the listeners
 *************************************************************************/
//////needs to be fixed
void Coach::play() {

    void * returnStatus;
    pthread_t newThreads[NUM_OF_THREADS];

    string openingMessage;
    //find the opening message
    do {
        openingMessage = client->mimicTcpReceive();

    } while (parseTitle(openingMessage)!= START);

    string command = "(eye on)";
    client->deliver(command.c_str(), command.size() + 1);

    //creates first thread for receiving messages
    int threadCheck = pthread_create(&newThreads[0], NULL,
            &Coach::launchReceive, this);
    //check that thread was created
    if (threadCheck) {
        cout << "thread number " << 1 << "failed with " << threadCheck << endl;
        exit(-1);
    }

    //create second thread for parsing the received messages
    threadCheck = pthread_create(&newThreads[1], NULL,
            &Coach::launchDeliver, this);
    //check that thread was created
    if (threadCheck) {
        cout << "thread number " << 2 << "failed with " << threadCheck << endl;
        exit(-1);
    }
    //wait for threads to finish
    for (int i = 0; i < 2; ++i) {
        threadCheck = pthread_join(newThreads[i], &returnStatus);
        if (threadCheck) {
            cout << "thread number " << i << "failed with " << threadCheck
                    << endl;
            exit(-1);
        }
    }

    pthread_exit(NULL);
}
//////might need another thread for notifying of strategy change

/*************************************************************************
 * function name:launchReceive*
 * The Input:thisObject-object to run its function in the thread *
 * The output:*
 * The Function operation:run the get function of thisObject in the thread*
 *************************************************************************/
void * Coach::launchReceive(void * thisObject) {

    Coach * obj = (Coach*) thisObject;
    return obj->get();
}
/*************************************************************************
 * function name:launchDeliver*
 * The Input:thisObject-object to run its function in the thread *
 * The output:*
 * The Function operation:run the send function of thisObject in the thread*
 *************************************************************************/
void * Coach::launchDeliver(void * thisObject) {

    Coach * obj = (Coach*) thisObject;
    return obj->send();
}

/*************************************************************************
 * function name:get()*
 * The Input: *
 * The output:*
 * The Function operation:get data from server as long as the game is running
 *************************************************************************/
void * Coach::get() {

    string text, title;
    do{
        //get data
         text = client->mimicTcpReceive();
         title = this->parseTitle(text);

         if ((title == SEE_GLOBAL) && !dataInUse) {
             //copy data so it can be analyzed
             seeGlobalData = text.substr();

             //data is now being used, lock
             dataInUse = true;
}
    } while(time!=GAME_OVER);

    pthread_exit(NULL);

}



/*************************************************************************
 * function name:send()*
 * The Input: *
 * The output:*
 * The Function operation:send data to server as long as the game is running
 *************************************************************************/
void * Coach::send() {

    while (true) { //change to while gme over

        cout<<"happy"<<endl;
    }

}

pair<double, double> Coach::parseBallLocation(const string text) {
    unsigned int pos = ZERO;
    string xPos, yPos;
    double x, y;
    pair <double, double> toReturn;
    unsigned int leftLim, rightLim, len;
    //finds the ball object in the see_global message
    leftLim = text.find(BALL);
    //pulls out its x coordinate
    pos = leftLim + ONE;
    rightLim = text.find(SPACE, pos);
    len = rightLim - leftLim;
    xPos = text.substr(leftLim, len);
    //and its y coordinate
    leftLim = rightLim + ONE;
    pos = leftLim;
    rightLim = text.find(SPACE, pos);
    yPos = text.substr(leftLim, len);
   //converts the coordinates from string to double
    x = atof(xPos.c_str());
    y = atof(yPos.c_str());

    //puts in a pair and returns:
    toReturn.first = x;
    toReturn.second = y;

    return toReturn;
}
