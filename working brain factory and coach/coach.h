#ifndef COACH_H
#define COACH_H
#include "udp.h"
#include "dataanalyzer.h"
#include "vector"

class Coach
{
//private:
public:
    UDP * client;
    bool offenseStrategy;
    unsigned int time;
    vector<DataAnalyzer *> observableList;
    static bool coachMade;
    bool dataInUse;
    string seeGlobalData;

    /*************************************************************************
    * function name:play*
    * The Input:                    *
    * The output:               *
    * The Function operation:opens 3 threads and manages the game*
    *************************************************************************/
    void play();



    /*************************************************************************
    * function name:launchReceive*
    * The Input:this*
    * The output:void * *
    * The Function operation:static function that launches a reaceiving thread*
    *************************************************************************/
    static void* launchReceive(void* thisObject);
    /*************************************************************************
    * function name:launchDeliver*
    * The Input:this*
    * The output:void * *
    * The Function operation:static function that launches a sending thread*
    *************************************************************************/
    static void* launchDeliver(void* thisObject);

    /*************************************************************************
    * function name:get*
    * The Input:              *
    * The output:void * *
    * The Function operation:handles input from server*
    *************************************************************************/
    void*  get();

    /*************************************************************************
    * function name:send*
    * The Input:              *
    * The output:void * *
    * The Function operation:sends the parsed data to brain*
    *************************************************************************/
    void * send();
    /*************************************************************************
    * function name:decideStrategy*
    * The Input:const string data*
    * The output:           *
    * The Function operation:decides on strategy base on data from server*
    *************************************************************************/
    void decideStrategy(const string data);
    /*************************************************************************
    * function name:receiveInput*
    * The Input:        *
    * The output:           *
    * The Function operation:receives input from server and decides on strategy*
    *************************************************************************/
    void receiveInput();

    /*************************************************************************
    * function name:notify*
    * The Input:        *
    * The output:           *
    * The Function operation:notifies all observers when strategy changes*
    *************************************************************************/
    void notify();
    /*************************************************************************
    * function name:Coach*
    * The Input:            *
    * The output:           *
    * The Function operation:deafult constructor*
    *************************************************************************/
    Coach();
//public:
    /*************************************************************************
    * function name:getCoach*
    * The Input:            *
    * The output:           *
    * The Function operation:makes a coach if there isn't already one*
    *************************************************************************/
    Coach * getCoach();
    /*************************************************************************
    * function name:signUp*
    * The Input:* DataAnalyzer;
    * The output:           *
    * The Function operation:signs up the observers and adds him to the observers list*
    *************************************************************************/
    void signUp(const DataAnalyzer * observers);



    /*************************************************************************
    * function name:parseTitle*
    * The Input:string*
    * The output:string*
    * The Function operation:returns the title of the message from server*
    *************************************************************************/
    string parseTitle(const string text);

    vector< pair<double, double> > parseTeamLocations (const string text);

    pair<double, double> parseBallLocation (const string text);

    bool isBallInOurTeam (vector <pair <double, double> >);

    double calcDistance (pair<double, double> player, pair<double, double> ball);

    ~Coach();
};

#endif // COACH_H
