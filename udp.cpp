/****************************************
* Student Name:Yonatan Azaria*
* Exercise Name:Multi-Threading*
* File description:UDP server and client*
****************************************/
#include "udp.h"



/*************************************************************************
* function name:UDP*
* The Input:port number and bool if server*
* The output:TCP object*
* The Function operation:makes a UDP server/client*
*************************************************************************/
UDP::UDP(int portNum, bool server)
{
    //set bool to know if object is server
    this->isServer = server;
    //set port number
    this->port = portNum;

    //bind socket for communcation
    this->sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (sock < 0) {
        cout <<"couldn't set socket\n";// error
    }
    //if socket could bind
    else
    {
        //init socket and port
        memset(&primarySockAddress, 0, sizeof(primarySockAddress));
        primarySockAddress.sin_family = AF_INET;
        primarySockAddress.sin_port = htons(this->port);

        //if object is server
        if (server)
        {
            //receive from any ip and bind socket
            primarySockAddress.sin_addr.s_addr = INADDR_ANY;
            if (bind(this->sock, (struct sockaddr *) &primarySockAddress, sizeof(primarySockAddress)) < 0) {
                cout << "couldn't bind socket\n";
            }
        }
        else
            //if client set ip to local host
            primarySockAddress.sin_addr.s_addr = inet_addr(IP_ADDRESS);
    }
}
/*************************************************************************
* function name:~UDP*
* The Input:              *
* The output:              *
* The Function operation:default destructor*
*************************************************************************/
UDP::~UDP()
{

}
/*************************************************************************
* function name:deliver*
* The Input:receives data in char[] format and size of data*
* The output:              *
* The Function operation:sends the data to the server/client using previosly set socket and port number*
*************************************************************************/
void UDP::deliver(const char sendData[], int dataLength)
{
    //send data throught the socket
    int bytesSent = sendto(this->sock, sendData, dataLength, 0, (struct sockaddr *) &primarySockAddress, sizeof(primarySockAddress));
    //check for errors
    if (bytesSent < 0) {
    cout <<"send error\n";// error
    }
    //else
      //  cout << "sent: "<< sendData <<"\n";

}
/*************************************************************************
* function name:receive*
* The Input:              *
* The output:returns received data in string format*
* The Function operation:returnes data from the server/client using previosly set socket and port number
* and sets socket for further communication*
*************************************************************************/
string UDP::receive()
{
    //length of address for receiving from client/server
    unsigned int serverReceiveAddressLen = sizeof(struct sockaddr_in);
    char buffer[BUFFER_MAX];
    //init buffer
    for (int i=0; i<(int)sizeof(buffer); ++i)
        buffer[i] = '\0';
    //receiver from and get address for further communications
    int bytes = recvfrom(this->sock, buffer, sizeof(buffer), 0, (struct sockaddr *) &serverReceiveAddress, &serverReceiveAddressLen);
    //check for errors
    if (bytes < 0) {
        cout <<"receive error\n";// error
    }
    else
        cout << buffer <<"\n";
    //if object is server - set the receiving address
    if (this->isServer)
        this->primarySockAddress = this->serverReceiveAddress;

    //make buffer into string and return
    string returnString(buffer);
    return returnString;

}
/*************************************************************************
* function name:receive*
* The Input:              *
* The output:returns received data in string format*
* The Function operation:using the receive function and mimics a server in order to keep communicating like TCP*
*************************************************************************/
string UDP::mimicTcpReceive()
{
    //mimic a server and get an address to reply to
    bool server = this->isServer;
    this->isServer = true;
    string returnString = this->receive();
    this->isServer = server;
    return returnString;

}
/*************************************************************************
* function name:finish*
* The Input:              *
* The output:              *
* The Function operation:closes the socket using close() function*
*************************************************************************/
void UDP::finish()
{
  close(this->sock);

}









