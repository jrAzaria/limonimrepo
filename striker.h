#ifndef STRIKER_H
#define STRIKER_H
#include "brain.h"

class Striker : public Brain
{
public:
    /*************************************************************************
    * function name:constructor*
    * The Input:int int int int string string string string*
    * The output:           *
    * The Function operation:makes a brain for a striker and make bounds, x & y for move, sets defense and offense points*
    *************************************************************************/
    Striker(pair<double, double> infX, pair<double, double> supX, pair<double, double> infY, pair<double, double> supY, string moveX, string moveY, pair<string, string> defense, pair<string,string> offense);

};

#endif // STRIKER_H
